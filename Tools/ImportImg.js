var fs = require('fs')
var assets_dir = "../Assets"
//導入圖片資源
const fsPromises = fs.promises;
const image_file_path = "../Component/ImgSources.js"
var used_img_name = [];


var end = {};
function Start_Scan() {
    return new Promise((resolve, reject) => {
        fs.readdir(assets_dir, (err, files) => {




            files.forEach(file => {

                var name = file.split("@")[0];
                if (used_img_name.indexOf(name) != -1) {
                    return;
                }
                used_img_name.push(name)
                var row = { 'uri': "require('../Assets/" + name + ".png')" }
                end[name] = row
            });



            resolve(end)


        })

    });
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }
function Write_to_File(){
    var string_json = JSON.stringify(end);
    string_json = replaceAll(string_json,'"r',"r");
    string_json = replaceAll(string_json,'"}','}');
    // console.log(string_json)
    var var_head = "  const ImgSources = "+string_json+";";
    
    var file_footer = '\nexport { ImgSources };';

    var file_content = var_head + file_footer;
    fs.writeFile(image_file_path, file_content, function (err) {
        if (err)
            console.log(err);
        else
            console.log("圖檔導入宣告完畢");
    });
}

Start_Scan().then(
(success) => { 
    
    // console.log(success)
    Write_to_File()

}, 
(failed) => { console.log(failed) });