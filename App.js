/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{
  useEffect,
  useState
} from 'react';

// import NotifService from './utils/NotifService';


import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Navigator,

} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import HomeScreen from './Pages/home.js';
import LoginScreen from './Pages/Login.js';
import RegisterScreen from './Pages/Register.js';
import Message_ListScreen from './Pages/Message_List.js'
import Member_PageScreen from './Pages/Member_Page.js'
import Fix_PageScreen from './Pages/Fix_Page.js'
import ChatRoom_Screen from './Pages/ChatRoom.js'
import Contact_Book_Screen from './Pages/Contact_book_Page.js'
import Post_Activity from './Pages/Post_Activity.js'
import PersonScreen from './Pages/Person_Setting.js'
import MemberUpgrade from './Pages/Member_Upgrade.js'
import PersonDetail from './Pages/Person_Detail'
import SEO_Setting from './Pages/SEO_Setting.js'
import Wallet_Setting from './Pages/Wallet_Setting.js'
import BV_Trans_SingUP from './Pages/BV_Trans_SingUP.js'

import BottomBar from "./Component/BottomBar.js"

import { NavigationContainer, useRoute } from '@react-navigation/native';
import { useHeaderHeight } from '@react-navigation/elements';


import * as eva from '@eva-design/eva';

import { ApplicationProvider } from '@ui-kitten/components';

import {SEO_Thread} from './Component/SEO_Thread_Services.js'

import { default as theme } from './Pages/custom-theme.json'; // <-- Import app theme
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "./Component/Host.js"


import messaging from '@react-native-firebase/messaging';

const Stack = createNativeStackNavigator();


const styles = StyleSheet.create({
  Header_Style: {
    backgroundColor: "#576274",
    height: 20
  },
  White_Header_Style: {
    backgroundColor: "#ffffff",
    height: 20
  },
  Hide_Header_Style: {

  },
  Header_Title_Font_Style: {

    fontSize: 15,

    color: "#ffffff"
  },
  White_Header_Title_Font_Style: {
    fontSize: 15,
    color: "#000"
  }
});

// const Haader_Options = {  headerTintColor: '#fff', headerStyle: styles.Header_Style, headerTitleStyle: styles.Header_Title_Font_Style }
const App = ({ navigation }) => {
  const [notif, setNotif] = useState(null);
  const [loading, setLoading] = useState(true);

  // useEffect(() => {
  //   notif || setNotif(NotifService);
  //   const asyncFunc = async () => {
  //     const hasOrderChannel = await notif.getChannelExists('orderChannel');
  //     hasOrderChannel || notif.createChannel('orderChannel');
  //     console.log('reg_NotifService')
  //   };
  //   notif && asyncFunc();
  // }, [notif]);

  useEffect(() => {
    messaging().getToken()
    .then(fcmToken => {
      if (fcmToken) {
        console.warn(fcmToken)
        Auth_Post_API("reg_device_token",{'token':fcmToken},function(e){
          console.log(e);
        })
        }});

    const unsubscribe = messaging().onMessage(async remoteMessage => {
      try {
        console.warn('推播訊息', JSON.stringify(remoteMessage));    
      } catch (error) {
        
      }
    
    });

    return unsubscribe;
  }, []);


  return (
    <ApplicationProvider {...eva} theme={{ ...eva.light, ...theme }}>
      <SafeAreaView style={{ flex: 1 }}>
        <NavigationContainer >

          <View style={{
            flex: 1,
            flexDirection: "column"
          }}>

<SEO_Thread></SEO_Thread>
            {/* <View style={{ flex: 2, backgroundColor: "red" }} /> */}
            <View style={{ flex: 20 }} >



              <Stack.Navigator initialRouteName="home" >
                <Stack.Screen name="home" key="home"
                  options={{ title: "首頁", headerTintColor: '#fff', headerStyle: styles.Header_Style, headerTitleStyle: styles.Header_Title_Font_Style }}
                  component={HomeScreen} />
                <Stack.Screen name="Register" key="Register" options={{ title: "註冊", headerTintColor: '#fff', headerStyle: styles.Header_Style, headerTitleStyle: styles.Header_Title_Font_Style }} component={RegisterScreen} />
                <Stack.Screen name="Login" key="Login" options={{ title: "登入", headerTintColor: '#fff', headerStyle: styles.Header_Style, headerTitleStyle: styles.Header_Title_Font_Style }} component={LoginScreen} />
                <Stack.Screen name="messenger" key="Messenger" options={{ title: "訊息", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={Message_ListScreen} />
                <Stack.Screen name="member" key="Member" options={{ title: "帳號設定", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={Member_PageScreen} />
                <Stack.Screen name="mai_sercive" key="mai_sercive" options={{ title: "SEO設定", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={SEO_Setting} />

                <Stack.Screen name="directory" key="Directory" options={{ title: "通訊錄", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={Contact_Book_Screen} />
                <Stack.Screen name="chatroom" key="chatroom" options={{ headerShown: false, title: "聊天室", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={ChatRoom_Screen} />
                <Stack.Screen name="post_activity" key="post_acti" options={{ title: "新增商業動態", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={Post_Activity} />
                <Stack.Screen name="person_detail" key="person_detail" options={{ title: "個人資料", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={PersonDetail} />
                <Stack.Screen name="wallet_setting" key="wallet_setting" options={{ title: "錢包設定", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={Wallet_Setting} />
                <Stack.Screen name="trans_bv" key="trans_bv" options={{ title: "轉出BV", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }} component={BV_Trans_SingUP} />



                <Stack.Screen name="person_setting" key="person_setting" options={{ title: "我的商務介紹", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }}
                  component={PersonScreen} />

                <Stack.Screen name="member_upgrade" key="member_upgrade" options={{ title: "鑽石/白金商務會員申請", headerTintColor: '#000', headerStyle: styles.White_Header_Style, headerTitleStyle: styles.White_Header_Title_Font_Style }}
                  component={MemberUpgrade} />

              </Stack.Navigator>




            </View>



            <View style={{ flex: 2 }}>

              <BottomBar />


            </View>


          </View>

        </NavigationContainer>
      </SafeAreaView>
    </ApplicationProvider>
  );

};

export default App;
