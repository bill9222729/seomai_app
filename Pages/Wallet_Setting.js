/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Linking
} from 'react-native';
import {
  Fumi,
} from "react-native-textinput-effects";

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { G_Styles } from "../Component/Global_Style.js"
import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js"
import { ImgSources } from "../Component/ImgSources.js"
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

import { Modal_Alert } from "../Component/Modal.js"
const base_info_sample = [
  { "column": "大頭貼", "val": ImgSources['avatar']['uri'], 'key': 'photo_url', 'val_type': "img", 'editable': true },
  { "column": "真實姓名/公司名稱", "val": "", 'key': 'name', 'editable': true },
  { "column": "手機號碼", "val": "", 'key': 'phone', 'editable': true },
  { "column": "E-Mail", "val": "", 'key': 'email', 'editable': true },
  { "column": "ID", "val": "", 'key': 'member_no', 'editable': false },
  { "column": "國家", "val": "", 'key': 'country', 'editable': false },
  { "column": "統一編號", "val": "", 'key': 'company_no', 'editable': true },
  { "column": "通訊地址", "val": "", 'key': 'address', 'editable': true },
  { "column": "聯絡電話#分機", "val": "", 'key': 'tel', 'editable': true },
  { "column": "聯絡人", "val": "", 'key': 'contact_name', 'editable': true },
  { "column": "部門", "val": "", 'key': 'part', 'editable': true },
  { "column": "職稱", "val": "", 'key': 'title', 'editable': true },
  { "column": "產業描述", "val": "", 'key': 'desc', 'editable': true },
]



const Wallet_Setting = ({ navigation }) => {
  const [wallet, setWallet] = React.useState(0);
  const [paper_photo,set_new_photo] = React.useState(null);

  function Load_Wallet() {

    function cab(e) {
      // console.log(e)
      var data = JSON.parse(e)['data'];
      setWallet(data['point'])
    }
    Auth_Get_API('my-wallet', cab);

  }


  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      // Load_User_Base_Info();
      Load_Wallet();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  //   React.useEffect(() => {
  // }, [base_info])



  return (
    <View style={{ flex: 1 }}>


      {/* <Modal_Alert show_stat={show_edit_modal} update_fn={set_Show_Edit_Modal} desc={edit_modal_desc} ok_callback={update_val} /> */}
      <ScrollView contentContainerStyle={{}}>

        <View style={{ flex: 1, backgroundColor: "#fff", alignItems: "center", paddingTop: 10, paddingBottom: 10 }}>
          <Text style={{ fontSize: 20, flex: 1 }}>{wallet} BV</Text>
          <Text style={{ fontSize: 10, flex: 1 }}>獎勵點數</Text>
        </View>

        <View>
          <TouchableOpacity

            style={[G_Styles.button, { marginTop: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center", alignItems: "center" }]}
            onPress={() => {
              navigation.navigate("trans_bv");
            }}
          >
            <Text

              style={{ color: "#000", marginRight: 10 }}>
              <FontAwesomeIcon name="money" size={15} color="#4F8EF7" style={{}} />

            </Text>
            <Text

              style={{ color: "#000" }}>

              轉出BV

            </Text>

          </TouchableOpacity>
        </View>
      </ScrollView>



    </View>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  container: {
    flex: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }, centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  okStyle: {
    padding: 10,
    borderRadius: 20
  }
});

export default Wallet_Setting;
