/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
  TextInput
} from 'react-native';

import { Post_API, Auth_Get_API, Auth_Post_API} from "../Component/Host.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { SeoMai_Inputer } from '../Component/Seomai_Inputer.js'
import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';

const Input_Map = [
  { 'title': "公司名稱", "placeholder": "請輸入公司名稱", "name": "company_name" ,"keyboardType":"default","required":true},
  { 'title': "通訊地址", "placeholder": "請輸入通訊地址", "name": "address" ,"keyboardType":"default","required":false},
  { 'title': "聯絡人(必填)", "placeholder": "請輸入聯絡人姓名", "name": "contact_name" ,"keyboardType":"default","required":true},
  { 'title': "E-mail(必填)", "placeholder": "請輸入E-Mail", "name": "email" ,"keyboardType":"email-address","required":true},
  { 'title': "手機(必填)", "placeholder": "請輸入手機號碼", "name": "phone","keyboardType":"numeric" ,"required":true},
  { 'title': "推薦人ID(必填)", "placeholder": "請輸入推薦人ID", "name": "ref_id" ,"keyboardType":"default","required":true},
  { 'title': "安置人ID", "placeholder": "請輸入安置人ID", "name": "place_id" ,"keyboardType":"default","required":false},

]


function Gen_Input_Keys(){
  var end = {};
  Input_Map.forEach(element => {
      var name = element.name;
      end[name] = "";
  });
  return end;
}


const Input_Key_Map = Gen_Input_Keys();


function Gen_Input_Map_Index (){
  var end = {};
  for (let index = 0; index < Input_Map.length; index++) {
    const element = Input_Map[index];
    var name = element.name;
    end[name] = index;
    
  }
  return end;
}
const Input_Map_Index = Gen_Input_Map_Index();



const MemberUpgrade = ({ navigation }) => {
  




  const [submit_info, setSubmit_Info] = useState(Input_Key_Map)

  const [ok_alert_show, setOk_alert] = useState(false);
  const [number, onChangeNumber] = React.useState(null);


  const [fail_alert_show, setFail_alert] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");
  const storeData = async (key, value) => {
    try {

      if (typeof (value) != "string") {
        value = JSON.stringify(value)
      }
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      console.log('save error')
      console.log(e)
      // saving error 
    }
  }



  function Submit() {

    var keys = Object.keys(submit_info);
    // console.log(keys);

    for (let index = 0; index < keys.length; index++) {
      var key = keys[index];
      var val = submit_info[key];
      
      var config_stat = Input_Map[Input_Map_Index[key]];
      // console.log(config_stat)

      if ( config_stat.required == true && val == "") {
        setFail_Desc(config_stat.title+"欄位不能留空!")
        setFail_alert(true);

        return 0;
      }

    }



    var callback = function (e) {
      console.log(e)
      var data = JSON.parse(e);
      console.log(data);
      if (data.success == true) {
        data = data['data'];
        // console.log(data)
        setOk_alert(true);
        // navigation.navigate("messenger");
      } else {

        setFail_Desc("送出失敗")
        setFail_alert(true);
      }

    }


    var fall_back = function (e) {
      // setOk_alert(true)
      console.log(e)

      setFail_Desc("送出失敗")
      setFail_alert(true);
    }
    // var post_data = { 'name': "new_test", "email": "email@gmail.com", "password": "123123123123123" };
    var post_data = submit_info;
    // console.log(post_data)
    Auth_Post_API("create_upgrade", post_data, callback, fall_back)

  }

  function Input_Update_text(name,val){
    submit_info[name] = val;
    setSubmit_Info(submit_info);
    // console.log(submit_info)
  }

  function Gen_Input_List() {

    var end = [];
    Input_Map.forEach(config_ele => {
      var row =
        <SeoMai_Inputer title={config_ele['title']} placeholder={config_ele['placeholder']} update_fn={Input_Update_text}  name={config_ele['name']} keyboardType={config_ele['keyboardType']} />
      end.push(row)
    });
    return end;

  }



  // console.log(G_Styles)
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 10 }]}>


        <Modal_Alert show_stat={fail_alert_show} update_fn={setFail_alert} desc={Fail_Desc} />


        <Modal_Alert show_stat={ok_alert_show} update_fn={setOk_alert} desc="送出成功！" />



        <View style={{}}>
          {Gen_Input_List()}

        </View>
        <View style={{ flex: 1 }}>

          <TouchableOpacity

            style={[G_Styles.button, { marginBottom: 20, backgroundColor: "#0093e8" }]}

            onPress={Submit}
          >
            <Text

              style={{ color: "#fff" }}>送出申請</Text>
          </TouchableOpacity>



        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  input: {
    height: 40,
    borderWidth: 0,
    padding: 10,
  },
});

export default MemberUpgrade;
