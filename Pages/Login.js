/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,


} from 'react-native';

import { Post_API,Auth_Post_API } from "../Component/Host.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';


import messaging from '@react-native-firebase/messaging';
const LoginScreen = ({ navigation }) => {
  const [account_info, setAccountInfo] = useState({ 'email': "", "password": "" })

  const [ok_alert_show, setOk_alert] = useState(false);

  const [fail_alert_show, setFail_alert] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");
  const storeData = async (key,value) => {
    try {
     
      if(typeof(value) != "string") {
        value = JSON.stringify(value)
      }
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      console.log('save error')
      console.log(e)
      // saving error
    }
  }
  function Login_User() {

    var keys = Object.keys(account_info);
    console.log(keys);

    for (let index = 0; index < keys.length; index++) {
      var key = keys[index];
      var val = account_info[key];
      // console.log(val)
      if (val == "") {


        setFail_Desc("欄位不能留空!")
        setFail_alert(true);

        // console.log(ok_alert_show)
        return 0;
      }

    }
    var callback = function (e) {
      var data = JSON.parse(e);
      if (data.success == true) {
        data = data['data'];
        // console.log(data)
        setOk_alert(true);
         storeData ('token',data['token'])
         storeData ('user',data['user'])




         messaging().getToken()
         .then(fcmToken => {
           if (fcmToken) {
             console.warn(fcmToken)
             Auth_Post_API("reg_device_token",{'token':fcmToken},function(e){
               console.log(e);
             })
             }});

             

         navigation.navigate("messenger"); 
      } else {

        setFail_Desc("登入失敗！")
        setFail_alert(true);
      }
      
    }


    var fall_back = function (e) {
      // setOk_alert(true)

      setFail_Desc("登入失敗！")
      setFail_alert(true);
    }
    // var post_data = { 'name': "new_test", "email": "email@gmail.com", "password": "123123123123123" };
    var post_data = account_info;
    // console.log(post_data)
    Post_API("login", post_data, callback, fall_back)

  }

  // console.log(G_Styles)
  return (
    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 10 }]}>


      <Modal_Alert show_stat={fail_alert_show} update_fn={setFail_alert} desc={Fail_Desc} />


      <Modal_Alert show_stat={ok_alert_show} update_fn={setOk_alert} desc="登入成功！" />
      <View style={[G_Styles.sub_box, { flex: 1 }]}>
        <Image
          source={ImgSources.logo01.uri}
        />

      </View>
      <View style={{ flex: 1 }}>
        <Fumi
          label={'電子郵箱'}
          iconClass={FontAwesomeIcon}
          style={[G_Styles.background_gary]}
          iconName={'user'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          onChangeText={(name) => updateForm_Info("email", { name }, setAccountInfo,account_info)}

          inputPadding={16}
        />
        <Fumi
          label={'密碼'}
          style={[G_Styles.background_gary]}
          iconClass={FontAwesomeIcon}
          iconName={'key'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          secureTextEntry={true}
          onChangeText={(name) => updateForm_Info("password", { name }, setAccountInfo,account_info)}

        />
      </View>
      <View style={{ flex: 1 }}>

        <TouchableOpacity

          style={[G_Styles.button, { marginBottom: 20, backgroundColor: "#0093e8" }]}

          onPress={Login_User}
        >
          <Text

            style={{ color: "#fff" }}>立即登入</Text>
        </TouchableOpacity>
        <TouchableOpacity

          style={[G_Styles.button, { marginBottom: 20, backgroundColor: '#373737' }]}
          onPress={() => {
            navigation.navigate('Register');
          }}
        >
          <Text
            style={{ color: "#fff" }}
          >會員快速註冊</Text>
        </TouchableOpacity>



      </View>
    </View>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },

});

export default LoginScreen;
