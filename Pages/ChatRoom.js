/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect, useRef } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  TextInput,
  Linking,
  PermissionsAndroid,
} from 'react-native';
import Images from 'react-native-chat-images';

import { useNavigationState } from '@react-navigation/native';
import { Post_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js";
import { Fixing_Box } from "../Component/Fixing.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';
import FastImage from 'react-native-fast-image'
import Hyperlink from 'react-native-hyperlink'


const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    // backgroundColor: 'pink',
    // marginHorizontal: 20,
  },
  text: {
    // fontSize: 42,
  },
  Stack_Title: {
    fontSize: 25,
    fontWeight: 'bold'
  },
  Last_Msg:
  {
    fontSize: 12,
    fontWeight: '100'

  },
  Person_Row: {
    width: '100%', height: 50, flexDirection: "row", borderBottomWidth: 2, borderBottomColor: "#ccc", marginTop: 10
  },
  PersonName: {
    fontSize: 18,
    paddingTop: 5,

  }
  , textInput: {
    color: '#000',
  },
});



const ChatRoom_Screen = ({ navigation }) => {
  function back_to_list() {

    navigation.navigate("messenger");

  }

  // 判斷字串是否為連結
  function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
  }




  const [Self_Info, set_Self_Info] = React.useState(null);
  const [Conver_Info, setConver_Info] = React.useState(null);
  const [is_First_load_Log, setFirst_Load_Log] = React.useState(false);
  const [Msg_Body, setMsg_Body] = React.useState("");
  const [Message_Log, setMessageLog] = useState([]);
  const [New_Photo, set_new_photo] = React.useState(null);
  const Scrollview_Ref = useRef(null);
  const [show_trans_modal, set_Trans_Modal] = useState(false)
  const [wallet, setWallet] = React.useState(0);
  const [want_trans_point, setWant_Point] = React.useState(0);
  const [alert_show_stat, setAlert_Show] = useState(false);
  const [Alert_Data, setAlert_Data] = useState("");

  function show_alert(desc) {
    setAlert_Data(desc);
    setAlert_Show(true);
  }
  // console.log("--------------------------------")
  // console.log(Self_Info)
  // console.log("--------------------------------")
  function Load_Wallet() {

    function cab(e) {
      // console.log(e)
      var data = JSON.parse(e)['data'];
      setWallet(data['point'])
    }
    Auth_Get_API('my-wallet', cab);

  }
  // Load_Wallet();
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {

      Load_Wallet();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);
  React.useEffect(() => {
    send_photo();
  }, [New_Photo]);




  //產生對話列
  function Message_Row(msg_item) {
    var self_data = Self_Info;
    var sender_data = msg_item.sender
    var receiver_data = msg_item.receiver
    var time_str = msg_item['created_at'].split(" ")[1]
    var time_str_arr = time_str.split(":");


    time_str = time_str_arr[0] + ":" + time_str_arr[1];
    // console.log(self_data)
    var is_RTL = false
    if (sender_data['id'] == self_data['id']) {
      is_RTL = true;
    }
    var msg_styles = StyleSheet.create({
      RTL_Msg: {
        backgroundColor: "#b9e3f9", paddingLeft: 20, paddingRight: 20, borderRadius: 4, borderTopRightRadius: 0, paddingTop: 5, paddingBottom: 10
      },
      TLR_Msg: {
        backgroundColor: "#fff", alignSelf: 'flex-start', paddingLeft: 20, paddingRight: 20, borderRadius: 4, borderTopLeftRadius: 0, paddingTop: 5, marginRight: 120, paddingBottom: 10
      },

    });
    if (is_RTL) {
      var work_style = msg_styles.RTL_Msg

    } else {
      var work_style = msg_styles.TLR_Msg
    }

    // Hyperlink可以判斷字串是否為連結然後做動作
    var body_render = (
      <Hyperlink onPress={(url, text) => Linking.openURL(url)} linkStyle={{ color: '#2980b9' }}>
        <Text style={work_style} selectable={true}>
          {msg_item['message']}
        </Text>
      </Hyperlink>
    );

    if (msg_item.message_type == 1) {
      // console.log(msg_item['file_name'])
      var uri = Host_URL + msg_item['file_name']

      // body_render = (<Image style={[work_style, { width: 100, height: 100 }]} source={{ uri: uri }} />)

      body_render = (
        <View style={[work_style, { width: 100 }]}>

          <Images backgroundColor='transparent' images={[uri]} />
        </View>)

    }

    if (is_RTL) {

      return (
        <View style={{ flex: 1, flexDirection: "row", marginBottom: 10, alignSelf: 'flex-end', marginLeft: 90 }}>
          <Text style={{ fontSize: 10, marginRight: 5, marginTop: 10 }}>{time_str}</Text>
          <View style={{ borderRadius: 8, padding: 10, paddingBottom: 0, paddingTop: 4 }}>
            <View style={{ alignSelf: 'flex-start' }}>
              {body_render}
            </View>
          </View>

          <TouchableOpacity onPress={() => {
            navigation.navigate('person_detail', {
              person_uid: msg_item['to_id'],
            });
          }}>
            <FastImage source={{ uri: sender_data['photo_url'] }} style={{ width: 30, marginLeft: 10, height: 30 }} />
          </TouchableOpacity>
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, flexDirection: "row", marginBottom: 10, alignSelf: 'flex-start' }}>
          <TouchableOpacity onPress={() => {
            navigation.navigate('person_detail', {
              person_uid: msg_item['from_id'],
            });
          }}>
            <FastImage source={{ uri: sender_data['photo_url'] }} style={{ width: 30, marginRight: 10, height: 30 }} />
          </TouchableOpacity>
          <View style={{ borderRadius: 8, padding: 10, paddingBottom: 0, paddingTop: 4 }}>
            {body_render}
          </View>
          <Text style={{ flex: 1, fontSize: 10, marginLeft: -120, marginTop: 10 }}>{time_str}</Text>
        </View>
      )
    }


  }

  // 向使用者請求使用相機的權限之後開啟相機，拍照後會直接把拍照的圖片傳到聊天室
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "開啟相機權限",
          message: "應用程式需要您的授權來開啟相機",
          buttonNeutral: "稍後問我",
          buttonNegative: "取消",
          buttonPositive: "同意"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Camera permission given");

        let options2 = {
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };

        launchCamera(options2, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            set_new_photo(response.assets[0])
          }
        });

      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };


  const selectImage = () => {
    const options = {
      noData: true,
      cameraType: "front",
    }
    // console.log(launchImageLibrary)
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        // const source = { uri: response.assets[0].uri }
        // console.log(response.assets[0])
        set_new_photo(response.assets[0])

        // setImagePreview(source)
        // setState("image", response.assets[0])
        // console.log(image_preview)
      }
    })
  }

  function Date_Row(day) {
    return (
      <View style={{ flex: 1, alignSelf: "center", margin: 10, backgroundColor: "#ccc", padding: 5, borderRadius: 4 }}>
        <Text style={{ fontSize: 10, color: "#fff" }}>{day}</Text>
      </View>
    )
  }

  var Last_Msg_Date = null;
  var Msg_UI = [];

  Message_Log.map(item => {
    var today = item['created_at'].split(" ")[0];

    if (Last_Msg_Date == null) {
      Last_Msg_Date = today
    } else {
      if (Last_Msg_Date != today) {
        Last_Msg_Date = today;
        // console.log(today)
        Msg_UI.push(Date_Row(today))
      }
    }

    Msg_UI.push(Message_Row(item))
    // return 
  })


  function Load_Message_Log() {
    var loaded_callback = (e) => {
      var data = JSON.parse(e)['data']['conversations'];
      // console.log(data);
      // console.log(typeof(data))
      data = data.reverse();
      // console.log(data)
      setMessageLog(data);
      setFirst_Load_Log(true)
    }

    var router_name = "users/" + Conver_Info['id'] + "/conversation";
    // console.log(router_name)
    Auth_Get_API("users/" + Conver_Info['id'] + "/conversation", loaded_callback)
  }

  function Load_Conver_Info() {
    AsyncStorage.getItem("conver_info").then((e) => {
      // console.log('load')
      console.log(e)
      var data = JSON.parse(e);
      setConver_Info(data)
      // console.log(Conver_Info)
    })
  }


  function Load_Self_Info() {
    AsyncStorage.getItem("user").then((e) => {
      // console.log('load')
      // console.log(e)
      var data = JSON.parse(e);
      set_Self_Info(data)

      // console.log(Conver_Info)
    })
  }


  if (Conver_Info == null) {
    Load_Conver_Info();
    Load_Self_Info();

    return (<View></View>)
  } else {
    if (is_First_load_Log == false) {
      Load_Message_Log();


      const room_msg_kepper = setInterval(() => {
        var nav_state = navigation.getState()
        var nav_index = nav_state['index']
        var nav_item = nav_state['routes'][nav_index]
        var nav_name = nav_item['name']
        if (nav_name == "chatroom") {
          Load_Message_Log();
        } else {
          clearInterval(room_msg_kepper);
        }
        //

      }, 2000);
    }

    // console.log(Conver_Info)
  }
  function send_photo() {
    if (Conver_Info == null) { return; }
    var post_data = { 'to_id': Conver_Info['id'], 'images': New_Photo }
    var url = "send-photo";
    var callback = function (e) {
      // console.log(e)
      Load_Message_Log()
    }
    Auth_Post_API(url, post_data, callback)
  }
  function send_msg() {

    var msg = Msg_Body['msg'];
    var post_data = { 'message': msg, 'to_id': Conver_Info['id'] }
    var url = "send-message";
    var callback = function (e) {
      // console.log(e)
      setMsg_Body("");
      Load_Message_Log();
    }
    Auth_Post_API(url, post_data, callback)

  }

  function Trans_Point() {

    set_Trans_Modal(true)


  }
  function Start_Trans() {
    // console.log(want_trans_point)
    if (want_trans_point == "0") {
      return 0;
    }
    if (want_trans_point > wallet) {
      show_alert("額度不足");
      return 0;
    }
    var post_data = {
      'recv_id': Conver_Info.id,
      'amount': want_trans_point,
      'reason': "APP 聊天室轉帳"
    }
    function callback(e) {
      // console.log(e)

      Load_Wallet();
      var data = JSON.parse(e);
      if (data['status']) {
        show_alert("轉帳成功!")
      }

    }
    Auth_Post_API("trans", post_data, callback);
  }
  function Trans_View() {

    return (
      <View>

        <Text style={styles.modalText}>請要贈送的CV數量 (目前餘額:{wallet})</Text>
        <Fumi
          label=''
          style={[styles.modalText, { width: 200 }]}
          iconClass={FontAwesomeIcon}
          iconName={'edit'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          keyboardType="numeric"
          defaultValue={want_trans_point}
          onChangeText={(val) => {
            // console.log(val)
            setWant_Point(val);
            // set_edit_item(item)
          }}

        />


      </View>
    )

  }
  var lv_img = <></>
  if (Conver_Info.lv == 'platinum') {
    lv_img = <Image source={ImgSources.platinum.uri} style={{ flex: 1, marginTop: 8 }} resizeMode="contain" />;
  }

  if (Conver_Info.lv == 'diamond') {
    lv_img = <Image source={ImgSources.diamond.uri} style={{ flex: 1, marginTop: 8 }} resizeMode="contain" />;
  }

  if (Conver_Info.lv == 'normal') {
    lv_img = <Image source={ImgSources.general.uri} style={{ flex: 1, marginTop: 8 }} resizeMode="contain" />;
  }


  var mai_btn = (<TouchableOpacity
    style={{ flex: 2, marginTop: -10, marginLeft: 20, height: 20 }}
    onPress={() => { Trans_Point() }}
  >
    <Image source={ImgSources.mai01.uri} style={{ width: 30, height: 27 }} resizeMode="contain" />

  </TouchableOpacity>

  )
  // if(Self_Info.lv == 'normal'){
  // mai_btn = <></>
  // }

  // console.log(G_Styles)
  if (Self_Info != null && Self_Info['lv'] == 'normal') {
    mai_btn = <></>
  } else {

  }
  return (


    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 0, padding: 0 }]}>

      <Modal_Alert show_stat={show_trans_modal} update_fn={set_Trans_Modal} desc={Trans_View()} ok_callback={Start_Trans} />

      <Modal_Alert show_stat={alert_show_stat} update_fn={setAlert_Show} desc={Alert_Data} />





      <View style={{ flex: 2, backgroundColor: "#fff", flexDirection: "row" }}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => { back_to_list() }}
        >
          <Text style={[styles.Stack_Title, { flex: 1 }]} >    <FontAwesomeIcon name="chevron-left" size={15} color="#4F8EF7" style={{ marginLeft: 10 }} /> </Text>
        </TouchableOpacity>
        {lv_img}
        <Text style={[styles.PersonName, { flex: 7 }]}>{Conver_Info['name']}</Text>
      </View>
      <View style={{ flex: 25, backgroundColor: "#e1ebf5" }}>


        <ScrollView contentContainerStyle={[styles.scrollView]}
          ref={Scrollview_Ref}
          onContentSizeChange={(width, height) => { Scrollview_Ref.current.scrollToEnd() }}
        >
          <View style={{ flex: 1, marginTop: 10, marginLeft: 10, marginRight: 10 }}>
            {Msg_UI}

          </View>


        </ScrollView>

      </View>
      <View style={{ flex: 2, backgroundColor: "#fff", flexDirection: "row", paddingTop: 30 }}>

        {/* 相機的按鈕 */}
        <TouchableOpacity
          style={{ flex: 1, marginTop: -10, marginLeft: 10, height: 20 }}
          onPress={() => { requestCameraPermission() }}
        >
          <Image source={ImgSources.camera01.uri} style={{ width: 30, height: 27 }} resizeMode="contain" />
        </TouchableOpacity>

        {/* 照片的按鈕 */}
        <TouchableOpacity
          style={{ flex: 1, marginTop: -10, marginLeft: 20, height: 20, marginRight: 10 }}
          onPress={() => { selectImage() }}
        >
          <Image source={ImgSources.photo01.uri} style={{ width: 30, height: 27 }} resizeMode="contain" />
        </TouchableOpacity>

        {mai_btn}
        <View style={{ flex: 1 }}></View>
        <TextInput
          style={[styles.textInput, { flex: 15, borderColor: 'gray', borderWidth: 1, borderRadius: 10, height: 50, marginTop: -20, fontSize: 15, paddingVertical: 0, paddingLeft: 20 }]}
          // Adding hint in TextInput using Placeholder option.
          placeholder="請輸入訊息..."
          onChangeText={(msg) => setMsg_Body({ msg })}
          // Making the Under line Transparent.
          underlineColorAndroid="transparent"
          value={Msg_Body['msg']}
        />
        <TouchableOpacity
          style={{ flex: 2, marginTop: 15, height: 20, marginRight: 10 }}
          onPress={() => { send_msg() }}
        >
          <Image source={ImgSources.arrow09.uri} style={{ width: 20, height: 20, marginLeft: 10, marginTop: -20 }} resizeMode="contain" />
        </TouchableOpacity>
      </View>
    </View>

  );
};



export default ChatRoom_Screen;
