/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
  TextInput
} from 'react-native';

import { Post_API, Auth_Get_API, Auth_Post_API } from "../Component/Host.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { SeoMai_Inputer } from '../Component/Seomai_Inputer.js'
import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';

const Input_Map = [
  { 'title': "金融機構代號(非必填)", "placeholder": "金融機構代號", "name": "bank_no", "keyboardType": "default", "required": false, 'input_type': "text" },
  { 'title': "銀行名稱", "placeholder": "銀行名稱", "name": "bank_name", "keyboardType": "default", "required": true, 'input_type': "text" },
  { 'title': "分行名稱", "placeholder": "分行名稱", "name": "sub_bank_name", "keyboardType": "default", "required": true, 'input_type': "text" },
  { 'title': "銀行戶名", "placeholder": "銀行戶名", "name": "account_name", "keyboardType": "email-address", "required": true, 'input_type': "text" },
  { 'title': "銀行帳號", "placeholder": "銀行帳號", "name": "account_no", "keyboardType": "numeric", "required": true, 'input_type': "text" },
  { 'title': "通訊地址", "placeholder": "通訊地址", "name": "address", "keyboardType": "default", "required": true, 'input_type': "text" },
  { 'title': "姓名/公司名稱", "placeholder": "姓名/公司名稱", "name": "contact_name", "keyboardType": "default", "required": true, 'input_type': "text" },
  { 'title': "身份証ID/統編/護照號碼", "placeholder": "身份証ID/統編/護照號碼", "name": "id_no", "keyboardType": "default", "required": true, 'input_type': "text" },
  { 'title': "上傳存摺封面", "placeholder": "上傳存摺封面", "name": "paper_photo", "keyboardType": "default", "required": true, 'input_type': "photo" },


]


function Gen_Input_Keys() {
  var end = {};
  Input_Map.forEach(element => {
    var name = element.name;
    end[name] = "";
  });
  return end;
}


const Input_Key_Map = Gen_Input_Keys();


function Gen_Input_Map_Index() {
  var end = {};
  for (let index = 0; index < Input_Map.length; index++) {
    const element = Input_Map[index];
    var name = element.name;
    end[name] = index;

  }
  return end;
}
const Input_Map_Index = Gen_Input_Map_Index();



const BV_Trans_SingUP = ({ navigation }) => {





  const [submit_info, setSubmit_Info] = useState(Input_Key_Map)

  const [ok_alert_show, setOk_alert] = useState(false);
  const [number, onChangeNumber] = React.useState(null);


  const [fail_alert_show, setFail_alert] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");

  const [wallet, setWallet] = React.useState(0);
  const [trans_point, setTrans_Point] = React.useState(0);
  const [end_point, setEnd_Point] = React.useState(0);
  
  function Load_Wallet() {

    function cab(e) {
      // console.log(e)
      var data = JSON.parse(e);
    }
    Auth_Get_API('my-wallet', cab);

  }


  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {

      Load_Wallet();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  React.useEffect(() => { 
    var ep = trans_point;
    ep = ep -3;
    if(ep <0){
      ep = 0;
    }
    if(ep>= wallet){
      ep = 0;
    }
    setEnd_Point(ep)

  }, [trans_point]);




  function Submit() {

    var keys = Object.keys(submit_info);
    // console.log(keys);

    for (let index = 0; index < keys.length; index++) {
      var key = keys[index];
      var val = submit_info[key];

      var config_stat = Input_Map[Input_Map_Index[key]];
      // console.log(config_stat)

      if (config_stat.required == true && val == "") {
        setFail_Desc(config_stat.title + "欄位不能留空!")
        setFail_alert(true);

        return 0;
      }

    }



    var callback = function (e) {
      console.log(e)
      var data = JSON.parse(e);
      console.log(data);
      if (data.success == true) {
        data = data['data'];
        // console.log(data)
        setOk_alert(true);
        // navigation.navigate("messenger");
      } else {

        setFail_Desc("送出失敗")
        setFail_alert(true);
      }

    }


    var fall_back = function (e) {
      // setOk_alert(true)
      console.log(e)

      setFail_Desc("送出失敗")
      setFail_alert(true);
    }
    // var post_data = { 'name': "new_test", "email": "email@gmail.com", "password": "123123123123123" };
    var post_data = submit_info;
    console.log(post_data)
    Auth_Post_API("trans_to_banks", post_data, callback, fall_back)

  }

  function Input_Update_text(name, val) {
    submit_info[name] = val;
    setSubmit_Info(submit_info);
    // console.log(submit_info)
  }

  function Gen_Input_List() {

    var end = [];
    Input_Map.forEach(config_ele => {
      if (config_ele['input_type'] == 'text') {
        var row =
          <SeoMai_Inputer title={config_ele['title']} placeholder={config_ele['placeholder']} update_fn={Input_Update_text} name={config_ele['name']} keyboardType={config_ele['keyboardType']} />
      } else {
        var row =
          <SeoMai_Inputer title={config_ele['title']} input_type={config_ele['input_type']} placeholder={config_ele['placeholder']} update_fn={Input_Update_text} name={config_ele['name']} keyboardType={config_ele['keyboardType']} />
      }

      end.push(row)
    });
    return end;

  }



  // console.log(G_Styles)
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={[G_Styles.background_gary, { paddingBottom: 10, backgroundColor: "#fff", padding: 10 }]}>


        <Modal_Alert show_stat={fail_alert_show} update_fn={setFail_alert} desc={Fail_Desc} />


        <Modal_Alert show_stat={ok_alert_show} update_fn={setOk_alert} desc="送出成功！" />
        <View style={{ paddingLeft: 10, borderBottomWidth: 0, borderBottomColor: "#dfe6ec", paddingBottom: 10 }}>
          <View style={{ flex: 1, backgroundColor: "#fff", flexDirection: "row", alignSelf: 'flex-start' }}>
            <Text style={{ flex: 1, color: "#0093e8", fontSize: 30 }}>BV</Text>
            

            <TextInput
              style={{ flex: 5, fontSize: 30 ,marginTop: -10 }}
                value={trans_point}
                onChangeText={setTrans_Point}
                placeholder="0"
                keyboardType="numeric"
            />
          </View>

          <Text style={{ flex: 1, fontSize: 15 ,color:"#000"}}>最多可轉 {wallet} BV</Text>

        </View>

        <View style={{ paddingLeft: 10, borderBottomWidth: 0, borderBottomColor: "#dfe6ec", paddingBottom: 10 ,marginBottom:10}}>
          <View style={{ flex: 1, backgroundColor: "#fff", flexDirection: "row", alignSelf: 'flex-start' }}>
            <Text style={{ flex: 1, color: "#000", fontSize: 15 }}>行政作業費：</Text>
            
            <Text style={{ flex: 2, color: "#0093e8", fontSize: 15 }}>3 BV</Text>
            
          </View>


        </View>
        <View style={{ paddingLeft: 10, borderBottomWidth: 3, borderBottomColor: "#dfe6ec", paddingBottom: 10 ,marginBottom:10}}>
          <View style={{ flex: 1, backgroundColor: "#fff", flexDirection: "row", alignSelf: 'flex-start' }}>
          <Text style={{ flex: 1, color: "#000", fontSize: 15 }}>轉出金額：</Text>
          <Text style={{ flex: 2, color: "#0093e8", fontSize: 15 }}>1 BV = NT$ 30</Text>
            
            
          </View>

          <Text style={{ flex: 1, color: "#ffae1b", fontSize: 30 }}>NT$ {end_point} BV</Text>

        </View>


        <View style={{ flex: 1 }}>
          {Gen_Input_List()}

        </View>
        <View style={{ flex: 1 }}>

          <TouchableOpacity

            style={[G_Styles.button, { marginBottom: 20, backgroundColor: "#0093e8" }]}

            onPress={Submit}
          >
            <Text

              style={{ color: "#fff" }}>送出申請</Text>
          </TouchableOpacity>



        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  input: {
    height: 40,
    borderWidth: 0,
    padding: 10,
  },
});

export default BV_Trans_SingUP;
