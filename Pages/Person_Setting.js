/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Linking
} from 'react-native';
import {
  Fumi,
} from "react-native-textinput-effects";

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { G_Styles } from "../Component/Global_Style.js"
import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js"
import { ImgSources } from "../Component/ImgSources.js"
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

import { Modal_Alert } from "../Component/Modal.js"
// const base_info_sample = [
//   { "column": "大頭貼", "val": ImgSources['avatar']['uri'], 'key': 'photo_url', 'val_type': "img", 'editable': true },
//   { "column": "真實姓名/公司名稱", "val": "", 'key': 'name', 'editable': true },
//   { "column": "手機號碼", "val": "", 'key': 'phone', 'editable': true },
//   { "column": "E-Mail", "val": "", 'key': 'email', 'editable': true },
//   { "column": "ID", "val": "", 'key': 'member_no', 'editable': false },
//   { "column": "國家", "val": "", 'key': 'country', 'editable': false },
//   { "column": "統一編號", "val": "", 'key': 'company_no', 'editable': true },
//   { "column": "通訊地址", "val": "", 'key': 'address', 'editable': true },
//   { "column": "聯絡電話#分機", "val": "", 'key': 'tel', 'editable': true },
//   { "column": "聯絡人", "val": "", 'key': 'contact_name', 'editable': true },
//   { "column": "部門", "val": "", 'key': 'part', 'editable': true },
//   { "column": "職稱", "val": "", 'key': 'title', 'editable': true },
//   { "column": "產業描述", "val": "", 'key': 'desc', 'editable': true },
// ]



const PersonScreen = ({ route, navigation }) => {

  const { init_data } = route.params;

  const [base_info, set_Base_Info] = React.useState(init_data);
  const [new_edit_val, set_new_val] = React.useState(null);
  const [edit_item, set_edit_item] = React.useState(null);
  const [show_edit_modal, set_Show_Edit_Modal] = React.useState(false);
  const [edit_modal_desc, set_Edit_Modal_Desc] = React.useState(false);
  const [new_photo, set_new_photo] = React.useState(null);

  const submit_photo = async () => {

    var post_data = { "data": new_photo }
    // console.log(post_data)
    Auth_Post_API("profile_upload_img", post_data, function (e) {
      console.log(e)
    })
  }

  React.useEffect(() => {
    if (new_photo != null) {
      submit_photo();
    }


  }, [new_photo]);


  const selectImage = () => {
    const options = {
      noData: true
    }
    // console.log(launchImageLibrary)
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.assets[0].uri }
        console.log(response.assets[0])
        set_new_photo(response.assets[0])

        // setImagePreview(source)
        // setState("image", response.assets[0])
        // console.log(image_preview)
      }
    })
  }
  function Load_User_Base_Info() {
    function fill_base_info(e) {
      var data = JSON.parse(e)['data']['user'];
      var new_info = base_info;
      for (let index = 0; index < base_info.length; index++) {
        var item = base_info[index];
        var key = item['key'];
        if (data.hasOwnProperty(key)) {
          // console.log(key)
          // console.log(data[key])
          // if(key == "photo_url"){
          //   data[key] = data[key].replace(/)
          // }
          new_info[index]['val'] = data[key];
        }
        // new_info[index]['val']

      }
      // console.log('loading done')
      // console.log(data)
      // console.log(new_info)
      set_Base_Info(new_info);
    }
    Auth_Get_API('profile', fill_base_info)
  }

  if (base_info['email'] == "") {

    Load_User_Base_Info();
  }
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      Load_User_Base_Info();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  function Gen_Menu_Row() {
    var n = 0;
    // console.log("Gen_Menu")
    var end = base_info.map(item => {
      n += 1;
      // console.log(typeof(item.val))
      var show_item = item.val;
      if (item.val != null && typeof (item.val) == "string") {
        if (item.val.length > 20) {
          show_item = item.val.substring(0, 20) + "...";
        }


      }

      var desc_data = <Text style={{ marginTop: 5, marginRight: 10 }} >{show_item}</Text>;

      if (item.hasOwnProperty("val_type")) {
        if (item['val_type'] == 'img') {

          try {
            if (item.val.indexOf("http") == -1) {

            } else {
              desc_data = <Image source={{ uri: item.val }} style={{ marginTop: 3, marginRight: 10, width: 30, height: 30 }}></Image>
            }
          } catch (error) {

          }

        }
      }

      // console.log(desc_data)

      var change_edit_stat = function (edit_item) {


        if (item['editable'] == false) {
          return 0;
        }

        if (item.hasOwnProperty("val_type")) {
          if (item['val_type'] == 'img') {
            selectImage()
          }

        } else {
          set_edit_item(item);


          set_Show_Edit_Modal(true);

          var desc_edit_box = (<View>
            <Text style={styles.modalText}>請輸入新的 {item['column']}</Text>

            <Fumi
              label=''
              style={[styles.modalText, { width: 200 }]}
              iconClass={FontAwesomeIcon}
              iconName={'edit'}
              iconColor={'#f95a25'}
              iconSize={20}
              iconWidth={40}
              inputPadding={16}
              keyboardType="default"
              defaultValue={item['val']}
              onChangeText={(name) => {
                item['val'] = name;
                set_edit_item(item)
              }}

            />
          </View>)
          set_Edit_Modal_Desc(desc_edit_box)
        }




      }
      return (<View key={n}><TouchableOpacity
        style={{ flexDirection: "row", backgroundColor: "#fff", height: 55, padding: 10, borderBottomColor: "#ccc", borderBottomWidth: 1 }}
        // onPress={() => { item['open_stat'] = !item['open_stat']; console.log(item['open_stat']) }}
        onPress={() => { change_edit_stat(edit_item) }}

      >
        <Text style={{ flex: 6, marginTop: 5, color: "#000", marginRight: 10 }}>{item.column}</Text>
        {desc_data}

      </TouchableOpacity>

      </View>);

    })
    return end;
  }
  function update_val() {
    // console.log(edit_item)
    var post_data = {}
    post_data[edit_item['key']] = edit_item['val']

    function cab(e) {
      // console.log(e)
    }
    Auth_Post_API("profile", post_data, cab)
  }
  var Menu_Rows = Gen_Menu_Row();
  React.useEffect(() => {
    console.log(base_info)
    Menu_Rows = Gen_Menu_Row();
  }, [base_info])



  return (
    <View style={{ flex: 1 }}>


      <Modal_Alert show_stat={show_edit_modal} update_fn={set_Show_Edit_Modal} desc={edit_modal_desc} ok_callback={update_val} />
      <ScrollView contentContainerStyle={{}}>

        <View style={G_Styles.container}>

          {Menu_Rows}
        </View>
      </ScrollView>



    </View>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  container: {
    flex: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }, centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  okStyle: {
    padding: 10,
    borderRadius: 20
  }
});

export default PersonScreen;
