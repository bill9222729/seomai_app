/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  StatusBar

} from 'react-native';

import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js"

import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { TabView, SceneMap,TabBar } from 'react-native-tab-view';

import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';
import FastImage from 'react-native-fast-image'

const base_info_sample = [
  { "column": "大頭貼", "val": ImgSources['avatar']['uri'], 'key': 'photo_url', 'val_type': "img", 'editable': true },
  { "column": "真實姓名/公司名稱", "val": "", 'key': 'name', 'editable': true },
  { "column": "手機號碼", "val": "", 'key': 'phone', 'editable': true },
  { "column": "E-Mail", "val": "", 'key': 'email', 'editable': true },
  { "column": "ID", "val": "", 'key': 'user_id', 'editable': false },
  { "column": "國家", "val": "", 'key': 'country', 'editable': false },
  { "column": "統一編號", "val": "", 'key': 'company_no', 'editable': true },
  { "column": "通訊地址", "val": "", 'key': 'address', 'editable': true },
  { "column": "聯絡電話#分機", "val": "", 'key': 'tel', 'editable': true },
  { "column": "聯絡人", "val": "", 'key': 'contact_name', 'editable': true },
  { "column": "部門", "val": "", 'key': 'part', 'editable': true },
  { "column": "職稱", "val": "", 'key': 'title', 'editable': true },
  { "column": "產業描述", "val": "", 'key': 'desc', 'editable': true },
]


const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    // backgroundColor: 'pink',
    // marginHorizontal: 20,
  },
  text: {
    // fontSize: 42,
  },
  Talk_Title: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  Last_Msg:
  {
    fontSize: 12,
    fontWeight: '100'

  },
  Person_Row: {
    width: '100%', height: 50, flexDirection: "row", borderBottomWidth: 2, borderBottomColor: "#ccc", marginTop: 10
  }

});
const Member_PageScreen = ({ navigation }) => {
  const [account_info, setAccountInfo] = useState({ 'email': "", "password": "" })
  const layout = useWindowDimensions();

  const [ok_alert_show, setOk_alert] = useState(false);

  const [fail_alert_show, setFail_alert] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");

  const [wallet_point, set_Wallet] = React.useState(0);

  const [wallet, setWallet] = React.useState(0);
  const [paper_photo, set_new_photo] = React.useState(null);
  const [base_info, set_Base_Info] = React.useState(base_info_sample);



  function Load_Wallet() {

    function cab(e) {
      console.log(e)
      var data = JSON.parse(e)['data'];
      setWallet(data['point'])
    }
    Auth_Get_API('my-wallet', cab);

  }

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {

      Load_Wallet();

    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const logout = () => {
    AsyncStorage.clear().then((response) => {

      navigation.navigate("Login");
    });

  }
  // console.log(G_Styles)
  return (
    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 0, padding: 0 }]}>


      <Modal_Alert show_stat={fail_alert_show} update_fn={setFail_alert} desc={Fail_Desc} />


      <Modal_Alert show_stat={ok_alert_show} update_fn={setOk_alert} desc="登入成功！" />
      <ScrollView contentContainerStyle={[styles.scrollView]}>
        <View style={{ flex: 1 }}>

          <View style={[G_Styles.button, { marginTop: 10, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center", alignItems: "center" }]}>
            <View style={{ flex: 1, backgroundColor: "#fff", alignItems: "center", paddingTop: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20, flex: 1 }}>{wallet} CV</Text>
              <Text style={{ fontSize: 10, flex: 1 }}>獎勵點數</Text>
            </View>

          </View>
          <TouchableOpacity

            style={[G_Styles.button, { marginTop: 20, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center", alignItems: "center" }]}
            onPress={() => {
              Auth_Get_API('profile', (e) => {
                var data = JSON.parse(e)['data']['user'];
                var new_info = base_info;
                for (let index = 0; index < base_info.length; index++) {
                  var item = base_info[index];
                  var key = item['key'];
                  if (data.hasOwnProperty(key)) {
                    // console.log(key)
                    // console.log(data[key])
                    // if(key == "photo_url"){
                    //   data[key] = data[key].replace(/)
                    // }
                    new_info[index]['val'] = data[key];
                  }
                  // new_info[index]['val']
                }
                navigation.navigate("person_setting", { init_data: base_info });
              })
            }}
          >
            <Text

              style={{ color: "#000", marginRight: 10 }}>
              <FontAwesomeIcon name="user" size={15} color="#4F8EF7" style={{}} />

            </Text>
            <Text

              style={{ color: "#000" }}>

              商務介紹設定

            </Text>

          </TouchableOpacity>

          {/* <TouchableOpacity

            style={[G_Styles.button, { marginTop: 10, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center", alignItems: "center" }]}
            onPress={() => {
              navigation.navigate("member_upgrade");
            }}
          >
            <Text

              style={{ color: "#000", marginRight: 10 }}>
              <FontAwesomeIcon name="arrow-up" size={15} color="#4F8EF7" style={{}} />

            </Text>
            <Text

              style={{ color: "#000" }}>

              會員升級申請

            </Text>

          </TouchableOpacity> */}


          <TouchableOpacity

            style={[G_Styles.button, { marginTop: 10, backgroundColor: "#fff", flexDirection: "row", justifyContent: "center", alignItems: "center" }]}
            onPress={() => {
              // navigation.navigate("wallet_setting");
            }}
          >

            <Text

              style={{ color: "#000", marginRight: 10 }}>
              <FontAwesomeIcon name="money" size={15} color="#4F8EF7" style={{}} />

            </Text>
            <Text

              style={{ color: "#000" }}>

              會員錢包設定

            </Text>

          </TouchableOpacity>

          <TouchableOpacity

            style={[G_Styles.button, { marginTop: 10, backgroundColor: "#fff" }]}
            onPress={logout}
          >
            <Text

              style={{ color: "#000" }}>
              <FontAwesomeIcon name="sign-out" size={15} color="#4F8EF7" style={{ marginLeft: 10 }} />
              會員登出

            </Text>

          </TouchableOpacity>



        </View>

      </ScrollView>

    </View>

  );
};



export default Member_PageScreen;
