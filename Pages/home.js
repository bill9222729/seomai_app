/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Linking
} from 'react-native';

import { G_Styles } from "../Component/Global_Style.js"
import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js"



const HomeScreen = ({navigation}) => {


  const [All_Img_List, setAll_Img] = React.useState([]);
  const [Home_is_loading_img, setHomeIsloadingImg] = React.useState(false)
  function Load_Data() {

    const image_list = [];
    var callback = function (e) {
      var all_data = JSON.parse(e);
      all_data = all_data.data;
      // console.log(all_data);


    
      for (let index = 0; index < all_data.slider.length; index++) {
        const element = all_data.slider[index];

        var url = Host_URL + element['path'].replace("/uploads", "uploads");



        var sample = (<View style={{ paddingTop: 10 }}  key={index}>
          <TouchableOpacity
         

            style={[]}
            onPress={() => { Linking.openURL(element['data']).catch(err => console.error("Couldn't load page", err)); }}
          >
            <Image
              source={{
                uri: url,
              }}
              style={{ width: 382, height: 200, marginBottom: 20 }}
            />
          </TouchableOpacity>
        </View>)

        image_list.push(sample)
      }
      // console.log(image_list)
      setAll_Img(image_list)
      setHomeIsloadingImg(true)

    }

    Auth_Get_API("get_home_msg", callback);
  }
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      Load_Data();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);
  if (Home_is_loading_img == false) {

  }

  //  console.log(G_Styles)
  return (
    <View style={G_Styles.container}>

      <View style={{ flex: 1, alignItems: "center" }}>
        <Text style={{ color: "#32425b", fontSize: 16 }}>
          商情資訊 NEW
        </Text>
      </View>
      <View style={{ flex: 10 }}>
        <ScrollView contentContainerStyle={{ alignItems: "center" }}>
          {All_Img_List}
        </ScrollView>
      </View>


    </View>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  container: {
    flex: 1,
  },
});

export default HomeScreen;
