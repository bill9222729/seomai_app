/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  TextInput,
  Modal,
  TouchableHighlight
} from 'react-native';
import { Post_API } from "../Component/Host.js";
import { G_Styles } from "../Component/Global_Style.js"
import { Modal_Alert } from "../Component/Modal.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from "react-native-textinput-effects";
import ModalSelector from 'react-native-modal-selector'

// const [state, setState] = useState(0);



let index = 0;
const country_list = [
  { key: "TW", label: '台灣' },
  { key: "VIE", label: '越南' },
  { key: "SGP", label: '新加坡' },
  { key: "MAW", label: '馬來西亞' },

];
const country_key_map = {}
country_list.forEach(element => {
  var key = element.key;
  var val = element.label;
  country_key_map[key] = val;
});



const RegisterScreen = ({ navigation } ) => {

  const [country, setCountry] = useState("TW");
  const [register_info, setRegInfo] = useState({ 'country': "TW", "name": "", "phone": "", "email": "", "password": "","password_com":"" })
  const [ok_alert_show, setOk_alert] = useState(false);
  const [fail_alert_show, setFail_alert] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");
  


  //  console.log(G_Styles)

  function after_success(){
    
    navigation.navigate('Login');
  }

  function register_user() {

    var keys = Object.keys(register_info);
    console.log(keys);

    for (let index = 0; index < keys.length; index++) {
      var key = keys[index];
      var val = register_info[key];
      // console.log(val)
      if (val == "") {
        
    
        setFail_Desc("欄位不能留空!")
        setFail_alert(true);
        // console.log(ok_alert_show)
        return 0;
      }

    }
    // console.log(register_info['password'].length)
    if(register_info['password'].length <6){
      setFail_Desc("密碼必須大於6碼！")
      setFail_alert(true);
      return 0;
    }
    if(register_info['password'] != register_info['password_com']){
      setFail_Desc("密碼不相符！")
      setFail_alert(true);
      return 0;
    }

    var callback = function (e) {
      var data = JSON.parse(e);
      if (data.success == true) {
        console.log('註冊成功')

        setOk_alert(true);
      } else { 
        setFail_Desc("註冊失敗！伺服器拒絕註冊！")
      setFail_alert(true);
      }
    }
    var fall_back = function (e) {
      // setOk_alert(true)
    
      setFail_Desc("註冊失敗！伺服器拒絕註冊！")
      setFail_alert(true);
    }
    // var post_data = { 'name': "new_test", "email": "email@gmail.com", "password": "123123123123123" };
    var post_data = register_info;
    console.log(post_data)
    Post_API("register", post_data, callback, fall_back)

  }



  function updateRegInfo(key, val) {
    var origin = register_info;
    
    var end_val = val;
    if(typeof(val) == 'object'){
      var first_key = Object.keys(val)[0];
      end_val = val[first_key];
    }
    origin[key] = end_val;
    setRegInfo(origin)
    if (key == "country") {
      // console.log("country:"+end_val)
      setCountry(end_val);
    }
    console.log(origin)
  }

  return (




    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 10 }]}>


      <Modal_Alert show_stat={ok_alert_show} update_fn={setOk_alert} ok_callback={after_success} desc="註冊成功！" />
      <Modal_Alert show_stat={fail_alert_show} update_fn={setFail_alert}  desc={Fail_Desc} />


      <View style={[G_Styles.sub_box, { flex: 1 }]}>


        <Text style={{ color: "#0093e8", textAlign: "left", width: "100%", fontSize: 25 }}>會員快速註冊</Text>
      </View>





      <View style={{ flex: 5, backgroundColor: "#ccc" }}>



        <Fumi
          label={'姓名'}
          iconClass={FontAwesomeIcon}
          style={[G_Styles.background_gary, { flex: 1 }]}
          iconName={'user'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          
          onChangeText={(name) => updateRegInfo("name", { name })}


        />

        <Fumi
          label={'手機號碼'}
          style={[G_Styles.background_gary, { flex: 1 }]}
          iconClass={FontAwesomeIcon}
          iconName={'phone'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          keyboardType="phone-pad"
          onChangeText={(name) => updateRegInfo("phone", { name })}

        />
        <ModalSelector
          data={country_list}
          initValue="Select something yummy!"
          supportedOrientations={['landscape']}
          accessible={true}
          scrollViewAccessibilityLabel={'Scrollable options'}
          cancelButtonAccessibilityLabel={'Cancel Button'}
          onChange={(option) => { updateRegInfo("country", option['key']) }}

        >

          <Fumi
            label={'國籍'}
            style={[G_Styles.background_gary, { padding: 10 }]}
            iconClass={FontAwesomeIcon}
            iconName={'globe'}
            iconColor={'#f95a25'}
            iconSize={20}
            iconWidth={40}
            inputPadding={16}
            value={country_key_map[country]}
          />
        </ModalSelector>



        <Fumi
          label={'E-mail'}
          style={[G_Styles.background_gary, { flex: 1 }]}
          iconClass={FontAwesomeIcon}
          iconName={'envelope'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          keyboardType="email-address"
          onChangeText={(name) => updateRegInfo("email", { name })}

        />

        <Fumi
          label={'密碼'}
          style={[G_Styles.background_gary, { flex: 1 }]}
          iconClass={FontAwesomeIcon}
          iconName={'key'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          secureTextEntry={true}
          onChangeText={(name) => updateRegInfo("password", { name })}
        />

        <Fumi
          label={'密碼確認'}
          style={[G_Styles.background_gary, { flex: 1 }]}
          iconClass={FontAwesomeIcon}
          iconName={'key'}
          iconColor={'#f95a25'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          secureTextEntry={true}
          
          onChangeText={(name) => updateRegInfo("password_com", { name })}
        />
      </View>
      <View style={{ flex: 1 }}>

        <TouchableOpacity

          style={[G_Styles.button, { marginTop: 20, backgroundColor: "#0093e8" }]}
          onPress={register_user}
        >
          <Text

            style={{ color: "#fff" }}>註冊</Text>
        </TouchableOpacity>



      </View>
    </View>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
});

export default RegisterScreen;
