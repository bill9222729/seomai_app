/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  useWindowDimensions,
  ScrollView,
  StatusBar

} from 'react-native';

import { Post_API, Auth_Get_API } from "../Component/Host.js";
import { EvaIconsPack } from '@ui-kitten/eva-icons';


import * as eva from '@eva-design/eva';

import { ApplicationProvider, Layout, Button, Text, Avatar, withStyles, List, IconRegistry, Icon } from '@ui-kitten/components';
import { Image, View, TouchableOpacity } from 'react-native';


import { default as theme } from './custom-theme.json'; // <-- Import app theme
const DATA = [
  {
    id: 1,
    user_name: "創世家",
    postTitle: 'Planet of Nature',
    avatarURI:
      'https://images.unsplash.com/photo-1559526323-cb2f2fe2591b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',

    randomText:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
  },
  {
    id: 2,
    user_name: "大長今",
    postTitle: 'Lampost',
    avatarURI:
      'https://images.unsplash.com/photo-1559526323-cb2f2fe2591b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
    imageURI:
      'https://images.unsplash.com/photo-1482822683622-00effad5052e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
    randomText:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
  }
]

const StarIcon = (props) => (
  <Icon {...props} name='plus-outline' fill='#FFF' />
);

const Business_Activity = (props) => {

  const [Activity_Data, setActivity] = useState(props.Activity_Data)
  const [is_render_done, set_is_render] = useState(false)

  // Load_Activity();



  // React.useEffect(() => {
  //   const unsubscribe = props.navigation.addListener('focus', () => {

  // Load_Activity();

  //   });

  //   // Return the function to unsubscribe from the event so it gets removed on unmount
  //   return unsubscribe;
  // }, [props.navigation]);

  // 點擊故事瀏覽次數就+1
  const add_story_view = (story_id) => {
    Auth_Get_API(`add-story-count/${story_id}`, (e) => {
      console.log(`故事編號${story_id}瀏覽次數+1`);
    });
  }

  const renderItems = ({ item }) => {
    // console.log(item)
    var img_render = <View></View>;
    if (item.hasOwnProperty("imageURI")) {
      img_render = <Image
        source={{ uri: item.imageURI }}
        style={themedStyle.cardImage}
      />
    }

    return (
      <TouchableOpacity activeOpacity={1} onPress={() => { add_story_view(item.story_id) }}>
        <View style={themedStyle.card}>
          <View style={themedStyle.cardHeader}>
            <TouchableOpacity
              style={{ flex: 1, flexDirection: "row" }}
              onPress={() => {
                props.navigation.navigate('person_detail', {
                  person_uid: item.id,

                });
              }}
            >
              <Avatar

                source={{ uri: item.avatarURI }}
                size='small'
                style={themedStyle.cardAvatar, { width: 30 }}
              />
              <Text category='s1' style={themedStyle.cardTitle, { flex: 1, paddingLeft: 10, paddingTop: 5 }}>
                {item.user_name}
              </Text>
            </TouchableOpacity>

          </View>
          {img_render}


          <View style={themedStyle.cardContent}>
            <Text category='p2'>{item.randomText}</Text>
          </View>
          <View style={themedStyle.cardContent, { flex: 1, flexDirection: "row", marginLeft: 10 }}>

            <Icon
              style={themedStyle.icon}
              fill='#8F9BB3'
              name='eye-outline'
            />
            <Text category='s2' style={{ flex: 1, marginLeft: 5, marginTop: -1, color: "#8F9BB3" }}>{item.views}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  };

  const List_Render_fn = function () {

    var rs = (<List
      style={themedStyle.container}

      data={Activity_Data}
      renderItem={renderItems}
    // keyExtractor={Activity_Data}
    />

    );

    return rs;

  }
  const List_Render = List_Render_fn();
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />

      {List_Render}

      <TouchableOpacity
        style={{ position: "absolute", right: 0, bottom: 0 }}
      >
        <Button style={themedStyle.button, {
          backgroundColor: "#0093ED", color: "#fff", borderRadius: 100, width: 25, shadowColor: 'rgba(0, 0, 0, 0.1)',
          shadowOpacity: 0.8,
          elevation: 6,
          shadowRadius: 15,
          shadowOffset: { width: 1, height: 13 },
          height: 75,
          width: 75,

        }}
          onPress={() => { props.navigation.navigate("post_activity"); }}
          appearance='ghost' status='danger' accessoryLeft={StarIcon} />
      </TouchableOpacity>
    </>
  );
};



export default Business_Activity;


const themedStyle = {
  container: {
    flex: 1
  }, button: {
    margin: 2,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 16,
    height: 16,
  },
  card: {
    backgroundColor: theme['color-basic-100'],
    marginBottom: 25,

    borderBottomWidth: 0.25,
  },
  cardImage: {
    width: '100%',
    height: 300
  },
  cardHeader: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cardTitle: {
    color: theme['color-basic-1000']
  },
  cardAvatar: {
    marginRight: 16
  },
  cardContent: {
    padding: 10,
    // borderWidth: 0.25,
    borderColor: theme['color-basic-600']
  },
  statusContent: {
    padding: 10,
    borderColor: theme['color-basic-600']
  }
}