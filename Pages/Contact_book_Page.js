/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  Modal,
  TouchableHighlight
} from 'react-native';

import { Post_API, Auth_Get_API, Auth_Post_API } from "../Component/Host.js";
import { Fixing_Box } from "../Component/Fixing.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';
import FastImage from 'react-native-fast-image'


const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    // backgroundColor: 'pink',
    // marginHorizontal: 20,
  },
  text: {
    // fontSize: 42,
  },
  Talk_Title: {
    fontSize: 15,
    fontWeight: 'bold',

  },
  Last_Msg:
  {
    fontSize: 12,
    fontWeight: '100'

  },
  Person_Row: {
    width: '100%', height: 50, flexDirection: "row", borderBottomWidth: 2, borderBottomColor: "#ccc", marginTop: 10
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }, centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  okStyle: {
    padding: 10,
    borderRadius: 20
  }

});

// const empty = Person_Row("","","")
// Person_List.push(empty)
const Fix_Render = Fixing_Box();


const _renderLabel = (scene) => {
  const normalStyle = { color: "#000" }
  const activeStyle = { color: "#0797e9" }
  // grab the label from the scene. I'm not really sure 
  // about the structure of scene, but you can see it using console.log
  const label = scene.route.title
  const focused = scene.focused;
  // console.log(scene)
  return (
    <Text
      style={[focused ? activeStyle : normalStyle]}
    >{label}</Text>
  );
}

const renderTabBar = props => (
  <TabBar
    {...props}
    renderLabel={_renderLabel}
    indicatorStyle={{ backgroundColor: '#0797e9' }}
    style={{ backgroundColor: '#fff' }}
  />
);




const Contact_Book_Screen = ({ navigation }) => {
  const layout = useWindowDimensions();

  const [ok_alert_show, setOk_alert] = useState(false);

  const [show_add_friend_modal, setFriend_Modalshow] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'Friend_list', title: '好友列表' },
    { key: 'Friend_Req', title: '談合作' },
    // { key: 'Business_msg', title: '談合作' },
  ]);
  const [FriendCount, setFriendCount] = useState(0)

  const [is_loading, setIsLoading] = React.useState(false);
  const [Chat_List, setChat_List] = useState([]);
  const [Friend_add_phone, setFriend_add_phone] = useState(null);
  const [add_done_show, setAddDone_Show] = useState(false);
  const [add_done_desc, setAdd_Done_Desc] = useState("");
  const [Friend_Req_List, setFriend_Req_List] = useState([]);
  const [member_data, setMember_data] = useState([]);


  // 開場先把會員自己的資料拉出來備用
  useEffect(() => {
    Auth_Get_API('profile', (e) => {
      var data = JSON.parse(e)['data']['user'];
      setMember_data(data);
      console.log(data);
    })
  }, [])



  function accept_friend(element) {
    var req_id = element['id'];
    console.log("accept_friend")
    console.log(req_id)
    var url = "accept-chat-request"
    var post_data = { 'id': req_id }
    var cab = function (e) {
      console.log(e)
      var data = JSON.parse(e)
      if (data.success) {
        setAdd_Done_Desc("好友關係已建立！")

      } else {
        setAdd_Done_Desc("送出失敗！")
      }

      Load_Conversations();
      Load_Friend_Req()


      setAddDone_Show(true)
    }
    Auth_Post_API(url, post_data, cab)
  }
  function show_detail(user_id) {
    navigation.navigate('person_detail', {
      person_uid: user_id,

    });
  }
  const Gen_Friend_Req_Row = () => {
    var end = [];
    if (Friend_Req_List.length == 0) {
      return [];
    }
    var country_map = { 'TW': "臺灣", "VIE": "越南", "SGP": "新加坡", "MAW": "馬來西亞" }
    for (let index = 0; index < Friend_Req_List.length; index++) {
      const element = Friend_Req_List[index];
      var row = (
        <TouchableOpacity
          onPress={() => { show_detail(element['user']['id']) }}
        >
          <View style={{ height: 70, width: "100%", padding: 10, paddingTop: 12, flexDirection: "row", borderBottomWidth: 1, borderBottomColor: "#ccc" }}>
            <Image style={{ height: 40, width: 40 }} source={{ uri: element['user']['photo_url'] }} />
            <View style={{ marginLeft: 10, marginTop: 5, flex: 4, fontSize: 12, fontWeight: "bold" }}>
              <Text >{element['user']['name']}</Text>
              <Text >{country_map[element['user']['country']]}</Text>

            </View>

            <TouchableOpacity

              style={[{ flex: 1, backgroundColor: "#0094e8", alignItems: "center", height: 30, paddingTop: 6, borderRadius: 30, marginTop: 10 }]}
              onPress={() => { accept_friend(element) }}
            >
              <Text

                style={{ color: "#fff" }}>同意</Text>
            </TouchableOpacity>



          </View>

        </TouchableOpacity>
      )
      end.push(row)

      // end.push(row)
    }
    // console.log(end)
    return end;
  }

  const Friend_Rows = Gen_Friend_Req_Row();
  const Friend_Req_Router = () => {
    return (
      <ScrollView style={{ flex: 1 }}>
        {Friend_Rows}
      </ScrollView>
    )
  };
  function Load_Friend_Req() {
    var cab = function (e) {
      setIsLoading(true)
      // console.log(e)
      var data = JSON.parse(e);


      var all_list = data['data'];
      setFriend_Req_List(all_list);
    }

    Auth_Get_API("my-chat-request", cab)
  }
  function go_to_chat(conver_info) {
    var conver_info_json = JSON.stringify(conver_info);
    // console.log(conver_info)
    AsyncStorage.setItem("conver_info", conver_info_json).then(() => {
      navigation.navigate("chatroom");
    })

  }
  function add_friend() {
    // 送出之前先檢查一下是不是自己的號碼
    let slef_phone_number = member_data['phone'];
    if (slef_phone_number === Friend_add_phone) {
      setAdd_Done_Desc("不能加自己好友喔~");
      setAddDone_Show(true);
      return;
    }


    // console.log(Friend_add_phone);
    if (Friend_add_phone != null) {

      var cab = function (e) {

        var data = JSON.parse(e)
        console.log(data)
        if (data.success) {
          setAdd_Done_Desc("加入成功！")
        } else {
          setAdd_Done_Desc("用戶不存在，或已經申請過好友！")
        }
        setAddDone_Show(true)
      }
      var post_data = { 'phone': Friend_add_phone, 'to_id': "0", 'message': "join us!" }
      Auth_Post_API("send-chat-request", post_data, cab)

    }

    setFriend_Modalshow(false);
  }
  const Add_Friend_Modal = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={show_add_friend_modal}
        onRequestClose={() => {
          // Alert.alert("Modal has been closed.");
          setFriend_Modalshow(!show_add_friend_modal);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>請輸入好友的聯絡電話：</Text>

            <Fumi
              label=''
              style={[styles.modalText, { width: 200 }]}
              iconClass={FontAwesomeIcon}
              iconName={'phone'}
              iconColor={'#f95a25'}
              iconSize={20}
              iconWidth={40}
              inputPadding={16}
              keyboardType="phone-pad"
              onChangeText={(name) => setFriend_add_phone(name)}

            />
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={add_friend}
            >
              <Text style={[styles.textStyle, styles.okStyle]}>OK!</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    )
  }
  const Person_Row = (icon_url, title, last_msg = "", conversations_info = null) => {

    var leng_max = 20;
    if (last_msg.length > leng_max) {
      last_msg = last_msg.substring(0, leng_max) + "...";
    }
    return (


      <View key={title} >
        <TouchableOpacity
          style={styles.Person_Row}
          onPress={() => { go_to_chat(conversations_info) }}
        >
          <View style={{ flex: 1, marginRight: 10 }}>

            <FastImage
              roundAsCircle={true}
              style={[{ width: "100%", height: "100%", borderRadius: 35 }]}
              source={{ uri: icon_url }}

              resizeMode="cover"
            />
          </View>
          <View style={{ flex: 7, flexDirection: "column" }}>
            <Text style={{ flex: 1, marginTop: 60 }, styles.Talk_Title}>{title}</Text>
            {/* <Text style={{ flex: 3 }, styles.Last_Msg}>{last_msg}</Text> */}

          </View>

        </TouchableOpacity>
      </View>

    )
  }


  function Load_Conversations() {
    function add_to_chat_list(data) {
      // console.log(data)
      var data = JSON.parse(data);
      data = data['data'];

      // Preload_User_List = data;
      setIsLoading(true);
      setChat_List(data['users']);
      // console.log(Chat_List);


    }

    Auth_Get_API("my-contacts", add_to_chat_list)
  }


  if (is_loading == false) {
    Load_Conversations();
    Load_Friend_Req()
    setInterval(function () {
      // console.log('loading')
      Load_Conversations();
      Load_Friend_Req();
    }, 500)
  } else {


    // console.log('user_list')
    // console.log(Chat_List)
  }

  const Person_List = [];

  for (let index = 0; index < Chat_List.length; index++) {
    var element = Chat_List[index];

    var row = Person_Row(element['photo_url'], element['name'], "", element)
    Person_List.push(row)
    // console.log(index)
  }
  const Friend_list_Router = () => (
    <View key="1" style={{ flex: 1 }}>


      <TouchableOpacity style={[{ backgroundColor: "#fff", height: 60 }]}

        onPress={() => { setFriend_Modalshow(true) }}
      >
        <View style={{ flex: 1, padding: 10, flexDirection: "row" }}>
          <Image source={ImgSources['add_friend'].uri} style={{ height: 30, width: 30 }}></Image>
          <Text style={{ flex: 1, fontSize: 20, fontWeight: "bold", marginLeft: 10 }}>新增朋友</Text>
          <Image source={ImgSources['arrow07'].uri} style={{ height: 15, width: 15, alignSelf: 'flex-end', marginBottom: 15 }}></Image>
        </View>
      </TouchableOpacity>
      <View style={[{ height: 40, backgroundColor: "#e1ebf5" }]}>
        <Text style={{ flex: 1, fontSize: 17, padding: 10 }}>好友 {FriendCount} </Text>
      </View>
      <ScrollView contentContainerStyle={[styles.scrollView]}>
        <View style={{ flex: 1, marginTop: 10, marginLeft: 10, marginRight: 10 }}>

          {Person_List}
        </View>


      </ScrollView>
    </View>
  );

  const renderScene = SceneMap({
    Friend_list: Friend_list_Router,
    Friend_Req: Friend_Req_Router,
  });


  // console.log(G_Styles)
  return (
    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 0, padding: 0 }]}>

      <Modal_Alert show_stat={add_done_show} desc={add_done_desc} update_fn={setAddDone_Show} />

      {Add_Friend_Modal()}


      <TabView
        style={{ flex: 1 }}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
      />

    </View>

  );
};



export default Contact_Book_Screen;
