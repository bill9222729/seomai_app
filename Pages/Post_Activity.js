/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  useWindowDimensions,
  ScrollView,
  StatusBar

} from 'react-native';

import { Post_API, Auth_Get_API,Auth_Post_API } from "../Component/Host.js";
import { EvaIconsPack } from '@ui-kitten/eva-icons';


import * as eva from '@eva-design/eva';

import { ApplicationProvider, Layout, Button, Text, Avatar, withStyles, List, IconRegistry, Icon, Input } from '@ui-kitten/components';
import { Image, View, TouchableOpacity } from 'react-native';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

import { default as theme } from './custom-theme.json'; // <-- Import app theme



const state = { image: null, title: '', description: '' }
function setState(key, title) {
  state[key] = title;
}
const onChangeTitle = title => {
  setState('title', title)
}
const onChangeDescription = description => {
  setState('description', description)
}



const Post_Activity = ({navigation}) => {
  
  const [image_preview,setImagePreview] = useState(null)
  const [is_uploading,setIsUploading] = useState(false)



  
const onSubmit = async () => {
  var post_data = {'content_type':"normal","data": state.image,"desc":state.description}
  // console.log(post_data)
  
  setIsUploading(true);
    Auth_Post_API("post_user_story",post_data,function(e){
      // console.log(e)
      
      setIsUploading(false);
      navigation.navigate("messenger");
    })
  }




const  Image_Preview_Render_fn = function(){
 
  if(image_preview == null){
    return <View></View>
  }
  return <Image source={image_preview} style={{width:"100%",height:300,borderWidth:1,backgroundColor:"#000",padding:10}} />
}
const Image_Preview_Render = Image_Preview_Render_fn()

// console.log("Render")
// console.log(Image_Preview_Render)
useEffect(() => {
 
},[image_preview])
const selectImage = () => {
  const options = {
    noData: true
  }
  // console.log(launchImageLibrary)
  launchImageLibrary(options, response => {
    if (response.didCancel) {
      console.log('User cancelled image picker')
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error)
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton)
    } else {
      const source = { uri: response.assets[0].uri }
      console.log(response.assets[0])
      setImagePreview(source)
      setState("image", response.assets[0])
      
      // console.log(image_preview)
    }
  })
}


var send_btn_fn = function() {
  // console.log(is_uploading)
  if(is_uploading == false){
    return (   <Button status='success' style={{ position: 'absolute',bottom:0}} onPress={onSubmit}>
    發佈商業動態
  </Button>)
  }else{
    return <View></View>
  }
}
const send_btn = send_btn_fn();

  return (
    <>
      <View style={{ flex: 1, padding: 10 }}>
        <View style={{ flex: 1, borderBottomWidth: 1, borderBottomColor: '#ccc' }}>
        {Image_Preview_Render}
          <Text style={{ marginBottom: 10 ,marginTop:20}}>動態貼文</Text>
          <Input placeholder="請輸入動態內容" multiline={true}
            style={{ height: 200, textAlignVertical: 'top', justifyContent: "flex-start" }}
            onChangeText={description => onChangeDescription(description)}
            numberOfLines={4} ></Input>
        </View>
        <View>
          {image_preview == null ?
        (<Button
          onPress={selectImage}
          style={{
            alignItems: 'center',
            padding: 10,
            margin: 30
          }}>
          上傳圖片
        </Button>)  :(<></>)

        }
        
        </View>
        <View style={{ marginTop: 80, alignItems: 'center' }}>
            
        {send_btn}
        </View>
      </View>
    </>
  );
};



export default Post_Activity;

const themedStyle = {
  container: {
    flex: 1
  }, button: {
    margin: 2,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 16,
    height: 16,
  },
  card: {
    backgroundColor: theme['color-basic-100'],
    marginBottom: 25,

    borderBottomWidth: 0.25,
  },
  cardImage: {
    width: '100%',
    height: 300
  },
  cardHeader: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cardTitle: {
    color: theme['color-basic-1000']
  },
  cardAvatar: {
    marginRight: 16
  },
  cardContent: {
    padding: 10,
    // borderWidth: 0.25,
    borderColor: theme['color-basic-600']
  },
  statusContent: {
    padding: 10,
    borderColor: theme['color-basic-600']
  }
}