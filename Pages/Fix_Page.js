/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  StatusBar

} from 'react-native';

import { Post_API, Auth_Get_API } from "../Component/Host.js";
import { Fixing_Box } from "../Component/Fixing.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';
import FastImage from 'react-native-fast-image'


const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    // backgroundColor: 'pink',
    // marginHorizontal: 20,
  },
  text: {
    // fontSize: 42,
  },
  Talk_Title: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  Last_Msg:
  {
    fontSize: 12,
    fontWeight: '100'

  },
  Person_Row: {
    width: '100%', height: 50, flexDirection: "row", borderBottomWidth: 2, borderBottomColor: "#ccc", marginTop: 10
  }

});






const Fix_PageScreen = ({ navigation }) => {
 

  // console.log(G_Styles)
  return (
    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 0, padding: 0 }]}>

    {Fixing_Box()}

    </View>

  );
};



export default Fix_PageScreen;
