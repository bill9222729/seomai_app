/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  Button,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  StatusBar

} from 'react-native';


import * as eva from '@eva-design/eva';

import { ApplicationProvider } from '@ui-kitten/components';
import { Tab, TabBar, Layout, TabView } from '@ui-kitten/components';

import { Post_API, Auth_Get_API } from "../Component/Host.js";
import { Fixing_Box } from "../Component/Fixing.js";
import { updateForm_Info } from "../Component/Global_Function.js"
import { G_Styles } from "../Component/Global_Style.js"
import { ImgSources } from "../Component/ImgSources.js"
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Modal_Alert } from "../Component/Modal.js"
import {
  Fumi,
} from "react-native-textinput-effects";

import AsyncStorage from '@react-native-async-storage/async-storage';
import FastImage from 'react-native-fast-image'

import Business_Activity from "./Business_Activity.js";
import { default as theme } from './custom-theme.json'; // <-- Import app theme

const styles = StyleSheet.create({
  tabContainer: {
    height: "100%"
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    // backgroundColor: 'pink',
    // marginHorizontal: 20,
  },
  text: {
    // fontSize: 42,
  },
  Talk_Title: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  Last_Msg:
  {
    fontSize: 12,
    fontWeight: '100'

  },
  Person_Row: {
    width: '100%', height: 50, flexDirection: "row", borderBottomWidth: 2, borderBottomColor: "#ccc", marginTop: 10
  }

});
// const empty = Person_Row("","","")
// Person_List.push(empty)
const Fix_Render = Fixing_Box();


const _renderLabel = (scene) => {
  const normalStyle = { color: "#000" }
  const activeStyle = { color: "#0797e9" }
  // grab the label from the scene. I'm not really sure 
  // about the structure of scene, but you can see it using console.log
  const label = scene.route.title
  const focused = scene.focused;
  // console.log(scene)
  return (
    <Text
      style={[focused ? activeStyle : normalStyle]}
    >{label}</Text>
  );
}

// const renderTabBar = props => (


//   <TabBar
//     {...props}
//     renderLabel={_renderLabel}
//     indicatorStyle={{ backgroundColor: '#0797e9' }}
//     style={{ backgroundColor: '#fff' }}
//   />
// );




const Message_ListScreen = ({ navigation }) => {
  const layout = useWindowDimensions();

  const [ok_alert_show, setOk_alert] = useState(false);

  const [fail_alert_show, setFail_alert] = useState(false);
  const [Fail_Desc, setFail_Desc] = useState("失敗！");

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'Chat', title: '聊天' },
    { key: 'Business_msg', title: '商業動態' },
  ]);
  const [PActivity, setPActivity] = React.useState(null);


  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const shouldLoadComponent = (index) => index === selectedIndex;


  const Load_Activity = function () {
    console.log("###################")

    if (PActivity != null) {

      // console.log(Activity_Data)
      return 0;
    } else {

      Auth_Get_API('user_stories', function (e) {
        e = JSON.parse(e);
        // console.log(e)
        var end = [];
        e.data.reverse();
        for (let index = 0; index < e.data.length; index++) {

          const element = e.data[index];
          var pack = { 'views': element['views'], 'story_id': element['id'], 'user_name': element['user_data']['name'], avatarURI: element['user_data']['photo_url'], imageURI: element['data'], randomText: element['desc'], 'id': element['user_id'] }
          end.push(pack)
        }




        setPActivity(end)
        // console.log(end)
        // props.setPActivity(end)
      })
    }



  }



  const SecondRoute = () => {
    return (
      // <View></View>
      <Business_Activity navigation={navigation} Activity_Data={PActivity} />
    )
  };
  const [message_is_loading, setMsgIsLoading] = React.useState(false);
  const [Chat_List, setChat_List] = useState([]);

  function go_to_chat(conver_info) {
    var conver_info_json = JSON.stringify(conver_info);
    AsyncStorage.setItem("conver_info", conver_info_json).then(() => {
      navigation.navigate("chatroom");
    })

  }
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      Load_Conversations();
      Load_Activity();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const Person_Row = (icon_url, title, last_msg_obj = null, conversations_info = null) => {
    // console.log("Call Person")
    // icon_url = "http://lorempixel.com/1920/1920/cats";
    // console.log(icon_url)
    // console.log(icon_url)
    var lv = conversations_info['lv'];
    var leng_max = 20;

    var last_msg = "";

    var last_msg_time = "";


    if (last_msg_obj != null) {
      last_msg = last_msg_obj['message'];
      var time_str = last_msg_obj['created_at'].split(" ")[1]
      var time_str_arr = time_str.split(":");
      last_msg_time = time_str_arr[0] + ":" + time_str_arr[1];

    }
    if (last_msg.length > leng_max) {
      last_msg = last_msg.substring(0, leng_max) + "...";
    }



    var lv_img = <></>
    if (lv == 'diamond') {
      lv_img = (<Image source={ImgSources.diamond.uri} style={{ width: 17, height: 20 }}></Image>)
    }
    if (lv == 'platinum') {
      lv_img = (<Image source={ImgSources.platinum.uri} style={{ width: 17, height: 20 }}></Image>)
    }
    if (lv == 'normal') {
      lv_img = (<Image source={ImgSources.general.uri} style={{ width: 17, height: 20 }}></Image>)
    }


    //  console.log(lv_img)

    return (


      <View key={title} >
        <TouchableOpacity
          style={styles.Person_Row}
          onPress={() => { go_to_chat(conversations_info) }}
        >
          <View style={{ flex: 1, marginRight: 10 }}>

            <FastImage
              roundAsCircle={true}
              style={[{ width: 35, height: 35, borderRadius: 35 }]}
              source={{ uri: icon_url }}

              resizeMode="cover"
            />

          </View>
          {lv_img}

          <View style={{ flex: 7, flexDirection: "column" }}>
            <Text style={{ flex: 1 }, styles.Talk_Title}>{title}</Text>
            <Text style={{ flex: 3 }, styles.Last_Msg} numberOfLines={1}>{last_msg}</Text>

          </View>
          <Text style={[styles.Last_Msg], { flex: 2, marginTop: 5 }}>{last_msg_time}</Text>

        </TouchableOpacity>
      </View>
    )
  }


  function Load_Conversations() {
    // console.log('load users')
    function add_to_chat_list(data) {
      // console.log(data)
      var data = JSON.parse(data);
      data = data['data'];

      // Preload_User_List = data;
      setMsgIsLoading(true);
      // console.log(data['users']);
      // setChat_List(data['users']);
      // console.log(Chat_List);

      // 以下又玄偷偷加工每一筆資料的last_message
      let all_user_conversation = data['users'];
      all_user_conversation.forEach((conversation, index) => {
        Auth_Get_API("users/" + conversation['id'] + "/conversation", (e) => {
          let current_conversations = JSON.parse(e)['data']['conversations'][0];
          all_user_conversation[index]['last_msg'] = current_conversations;

          // 這邊每去請求一筆就會setState一次，當訊息數一多效率會非常的差，比較好的解法應該是直接去後端把"my-contacts"這隻api改好
          setChat_List(all_user_conversation);
        });
      });


    }

    Auth_Get_API("my-contacts", add_to_chat_list)
  }


  if (message_is_loading == false) {
    Load_Conversations();
    setInterval(
      () => {
        Load_Conversations();
      }
      , 3000)
  } else {

    // console.log('user_list')
    // console.log(Chat_List)
  }
  // console.log(message_is_loading)

  const Person_List = [];

  for (let index = 0; index < Chat_List.length; index++) {
    var element = Chat_List[index];
    // console.log(element)
    var row = Person_Row(element['photo_url'], element['name'], element['last_msg'], element)
    Person_List.push(row)
    // console.log(index)
  }
  const FirstRoute = () => (
    <View key="1" style={{ flex: 1 }}>

      <ScrollView contentContainerStyle={[styles.scrollView]}>
        <View style={{ flex: 1, marginTop: 10, marginLeft: 10, marginRight: 10 }}>

          {Person_List}
        </View>


      </ScrollView>
    </View>
  );
  // const renderScene = SceneMap({
  //   Chat: FirstRoute,
  //   Business_msg: SecondRoute,
  // });

  const business_active = SecondRoute();
  // console.log(business_active)
  // console.log(G_Styles)
  return (

    <View style={[G_Styles.container, G_Styles.background_gary, { paddingBottom: 0, padding: 0 }]}>


      <Modal_Alert show_stat={fail_alert_show} update_fn={setFail_alert} desc={Fail_Desc} />


      <Modal_Alert show_stat={ok_alert_show} update_fn={setOk_alert} desc="登入成功！" />
      {/* <TabView
        style={{ flex: 1 }}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
      /> */}
      {/* <TabView
      selectedIndex={selectedIndex}
      onSelect={index => setSelectedIndex(index)}>
      <Tab title='USERS'>
        <Layout style={styles.tabContainer}>
          <Text category='h5'>USERS</Text>
        </Layout>
      </Tab>
      <Tab title='ORDERS'>
        <Layout style={styles.tabContainer}>
          <Text category='h5'>ORDERS</Text>
        </Layout>
      </Tab>
      <Tab title='TRANSACTIONS'>
        <Layout style={styles.tabContainer}>
          <Text category='h5'>TRANSACTIONS</Text>
        </Layout>
      </Tab>
    </TabView> */}

      <TabView
        selectedIndex={selectedIndex}
        onSelect={index => setSelectedIndex(index)}
        shouldLoadComponent={shouldLoadComponent}
        style={{ flex: 1 }}
      >
        <Tab title='聊天' style={{ height: 40 }}>
          <Layout style={styles.tabContainer}>
            {FirstRoute()}
          </Layout>

        </Tab>
        <Tab title='商業動態' style={{ height: 40 }}>
          <Layout style={styles.tabContainer}>

            {business_active}
          </Layout>
        </Tab>
      </TabView>



    </View>
  );
};



export default Message_ListScreen;
