/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  Linking

} from 'react-native';

import { G_Styles } from "../Component/Global_Style.js"
import { EvaIconsPack } from '@ui-kitten/eva-icons';


import * as eva from '@eva-design/eva';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { ApplicationProvider, Layout, Button, Text, Avatar, Modal, Input, IconRegistry, Icon, Card } from '@ui-kitten/components';
import { Image, View, TouchableOpacity } from 'react-native';

import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js"

import { default as theme } from './custom-theme.json'; // <-- Import app theme

import { Modal_Alert } from "../Component/Modal.js"
const SEO_Setting = ({ navigation }) => {
  const [visible, setVisible] = React.useState(false);

  const [Activity_Data, setActivity] = useState(null)
  const [now_fill_url, setNow_Fill_Url] = useState(null)
  const [all_img, setAll_Img] = useState([])
  const [now_thread_num, setNow_Thread_Num] = useState(null)
  const [free_thread, setFree_Thread] = useState([])
  const [pay_thread, setPay_Thread] = useState([])

  const [all_thread, setAll_Thread] = useState(null)
  const [alert_show_stat, setAlert_Show] = useState(false);
  const [Alert_Data, setAlert_Data] = useState("");
  const [show_tread_buy, setShowTread_buy] = useState(false);
  const [want_buy_thread_num, setWantBuy_Thread] = useState(0);
  const [seo_work_list, setSEO_Work_List] = useState([]);
  const [now_work_seo_url, setNow_Work_URL] = useState(null);
  const [seo_num, setSEO_Num] = useState(-1);
  function show_alert(desc) {
    setAlert_Data(desc);
    setAlert_Show(true);
  }

  const [wallet, setWallet] = React.useState(0);
  const [paper_photo, set_new_photo] = React.useState(null);


  function Load_Seo_Status() {
    AsyncStorage.getItem('seo_list').then((token) => {
      // console.log('load  seo_worker',token);
      if (token !== null) {
        setSEO_Work_List(JSON.parse(token))

      }
    })
    AsyncStorage.getItem('now_work_seo_url').then((token) => {
      // console.log('load  seo_worker',token);
      if (token !== null) {

        setNow_Work_URL(token);
        // console.log('load now work url ',token)
      }
    })


    AsyncStorage.getItem('seo_num').then((token) => {
      // console.log('load  seo_num',token);
      if (token !== null) {
        setSEO_Num(token)

      }
    })
  }



  function Load_Wallet() {

    function cab(e) {
      // console.log(e)
      var data = JSON.parse(e)['data'];
      setWallet(data['point'])
    }
    Auth_Get_API('my-wallet', cab);

  }

  function Load_Data() {

    const image_list = [];
    var callback = function (e) {
      var all_data = JSON.parse(e);
      all_data = all_data.data;
      console.log(all_data);

      for (let index = 0; index < all_data.slider.length; index++) {
        const element = all_data.slider[index];

        var url = Host_URL + element['path'].replace("/uploads", "uploads");



        var sample = (<View style={{ paddingTop: 0 }}>
          <TouchableOpacity

            style={[]}
            onPress={() => { Linking.openURL(element['data']).catch(err => console.error("Couldn't load page", err)); }}
          >
            <Image
              source={{
                uri: url,
              }}
              style={{ width: 400, height: 200 }}
            />
          </TouchableOpacity>
        </View>)

        image_list.push(sample)
        break;
      }
      // console.log(image_list)
      setAll_Img(image_list)
      // setHomeIsloadingImg(true)

    }

    Auth_Get_API("get_home_msg", callback);
  }

  React.useEffect(() => {


  }, [free_thread, pay_thread])


  React.useEffect(() => {

    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      Load_Seo_Status();
      Load_Data();
      Load_Wallet();
      if (free_thread.length == 0) {
        setInterval(() => {

          Load_All_Thread();
        }, 1000)


        setInterval(() => {

          Load_Seo_Status();
        }, 1000)

      }
      return unsubscribe;

    });
  }, [navigation])


  const render_img_fn = function () {

    if (all_img.length == 0) {
      return <></>
    } else {
      return all_img[0]
    }
  }

  const Clean_thread = function (thread_type, thread_num) {
    try {
      setNow_Thread_Num({ 'type': thread_type, 'num': thread_num });

      setNow_Fill_Url("");
      store_thread();

    } catch (error) {
      console.log("Clean Error:")
      console.log(error);

    }

    // setTimeout(function(){

    //   Load_All_Thread();
    // },1000)

  }
  const edit_thread = function (thread_type, thread_num) {

    // var post_data = {'types':thread_type,'num':thread_id};

    // Auth_Post_API("seo_thread_edit",post_data)

    setNow_Thread_Num({ 'type': thread_type, 'num': thread_num });
    setVisible(true)
  }
  const store_thread = function () {
    // if(now_fill_url.indexOf('http')==-1){
    //   return 0;
    // }
    var post_data = now_thread_num;
    //  console.log(post_data)
    post_data['url'] = now_fill_url;

    function cab(e) {
      // console.log(e);
      setVisible(false);
      Load_All_Thread();
    }

    Auth_Post_API("seo_thread_edit", post_data, cab)


    // var thread_name = "thread_"+now_thread_num;
    // storeData(thread_name,now_fill_url);
    // setVisible(false)
  }

  const Gen_Thread_Render = function (thread_config, num, only_result = false, is_now_working = false) {
    var thread_url = thread_config['url'];
    // var thread_name = "thread_"+thread_config['id'];
    // console.log(thread_name);
    // console.log(thread_num)
    var url_txt = <Text category='c1' style={{ flex: 8 }}>尚未設定</Text>
    // console.log(all_thread)
    // console.log(all_thread[thread_num-1])
    if (thread_url != null) {
      url_txt = <Text category='c1' style={{ flex: 8 }}>{thread_url}</Text>
    }

    var thread_type = "免費線程";
    if (thread_config['types'] != "free") {
      thread_type = "付費線程 到期日：" + thread_config['exp_date'];
    }

    var result_view = <></>;
    if (only_result == true) {
      var last_n = thread_url.length;
      if (last_n > 18) {
        last_n = 18;

      }
      thread_url = thread_url.slice(0, last_n);
      thread_url = thread_url + "*****";

      url_txt = <Text category='c1' style={{ flex: 8 }}>{thread_url}</Text>
      var icon_ui = <Icon
        style={themedStyle.icon}
        fill='#8F9BB3'
        name='arrow-right-outline'
      />;
      if (is_now_working) {
        icon_ui = <Icon
          style={themedStyle.icon}
          fill='#0093e8'
          name='arrow-right-outline'
        />
      }

      thread_type = <></>
      // console.log(store_item)
      return <View style={{ borderBottomWidth: 1, borderBottomColor: "#ccc", paddingBottom: 10, marginTop: -20 }}>
        <Text category="c1">{thread_type}</Text>
        <View style={{ marginTop: 10 }}>
          <TouchableOpacity style={{ flex: 10, paddingLeft: 10, flexDirection: "row" }}

            onPress={() => { edit_thread(thread_config['types'], num - 1) }}
          >

            {icon_ui}
            {url_txt}

          </TouchableOpacity>

          {result_view}
        </View>
      </View>
    } else {
      // console.log(store_item)
      return <View style={{ borderBottomWidth: 1, borderBottomColor: "#ccc", paddingBottom: 10 }}>
        <Text category="c1">{thread_type}</Text>
        <View style={{ marginTop: 10 }}>
          <TouchableOpacity style={{ flex: 10, paddingLeft: 10, flexDirection: "row" }}

            onPress={() => { edit_thread(thread_config['types'], num - 1) }}
          >
            <Icon
              style={themedStyle.icon}
              fill='#8F9BB3'
              name='globe-outline'
            />

            {url_txt}
            <TouchableOpacity

              style={[G_Styles.button, { marginTop: -10, flex: 1, backgroundColor: "#fff", }]}

              onPress={() => { Clean_thread(thread_config['types'], num - 1) }}
            >
              <Text

                style={{ flex: 3, color: "#ccc" }}>DEL</Text>
            </TouchableOpacity>
          </TouchableOpacity>

          {result_view}
        </View>
      </View>
    }

  }

  const thread_keys = [1, 2, 3]

  async function Load_All_Thread() {

    function callback(e) {
      // console.log(e);
      var data = JSON.parse(e)['data'][0];
      // console.log("AAAAAAAAA");
      // console.log(data);
      // console.log(data)
      var ft = data['free_thread']
      // console.log(ft)\
      setFree_Thread(JSON.parse(ft))
      var ot = data['payment_thread']
      setPay_Thread(JSON.parse(ot));
    }


    Auth_Get_API("seo_thread_managers", callback);




  }
  var n = 0;
  var Thread_Rows = [];
  free_thread.map(function (e) {
    n++;
    var rs = Gen_Thread_Render(e, n);
    Thread_Rows.push(rs)
  })

  var n = 0;
  pay_thread.map(function (e) {
    n++;
    var rs = Gen_Thread_Render(e, n);
    Thread_Rows.push(rs)
  })


  function Render_Thread_Result() {
    var end = [];
    var all_thread = [];
    // free_thread.forEach(element => {
    //   all_thread.push(element);
    // });

    // pay_thread.forEach(element => {
    //   all_thread.push(element);
    // });

    // const [seo_work_list, setSEO_Work_List] = useState([]);
    // const [seo_num, setSEO_Num] = useState(-1);

    var n = 0;
    seo_work_list.forEach(element => {
      var is_now_working = false;
      if (n == now_work_seo_url) {
        is_now_working = true;
      }
      var base_table = Gen_Thread_Render(element, 0, true, is_now_working);
      end.push(base_table);
      n++;
    });
    // console.log(end)
    // console.log(seo_work_list)
    return end;
  }

  const Thread_Result = Render_Thread_Result();


  function Buy_Thread() {
    setShowTread_buy(true)
  }
  function Send_Buy_Thread() {
    if (parseInt(want_buy_thread_num) == 0) {
      show_alert("數量不能為0");
      return 0;
    }
    var tt = want_buy_thread_num * 330;
    setShowTread_buy(false)
    // console.log(tt,wallet)
    if (tt > wallet) {
      show_alert("額度不足");
      return 0;
    }


    function cab(e) {
      // console.log(e);
    }
    console.log(want_buy_thread_num);
    var post_data = { 'num': want_buy_thread_num }
    Auth_Post_API('buy_seo_thread', post_data, cab)



  }

  // function Render_Thread_RunWoker(){

  //   var end = [];

  //   return <View style={{ borderBottomWidth: 1, borderBottomColor: "#ccc", paddingBottom: 10 }}>
  //   <Text category="c1">{thread_type}</Text>
  //   <View style={{ marginTop: 10 }}>


  //       {url_txt}
  //     </TouchableOpacity>

  //     {result_view}
  //   </View>
  // </View>;

  // }
  // const Last_Run_Worker = Render_Thread_RunWoker();

  function Thread_Buy_Render() {
    Load_Wallet();
    return (
      <View>
        <Text> 填入要購買的線程數量</Text>
        <Text> 目前CV額度：<Text style={{ color: "#4F8EF7" }}>{wallet}</Text></Text>
        <Text> 每線程需要：<Text style={{ color: "#4F8EF7" }}>330點CV</Text></Text>
        <Input placeholder="1" multiline={false} keyboardType="numeric" defaultValue="0"
          style={{ height: 50, width: 200, textAlignVertical: 'top', justifyContent: "flex-start" }}
          onChangeText={description => setWantBuy_Thread(description)}
        ></Input>
        <Text style={{ paddingBottom: 20 }}> 小計：<Text style={{ color: "#4F8EF7" }}>{want_buy_thread_num * 330}點CV</Text></Text>
      </View>
    )
  }


  return (
    <>

      <Modal_Alert show_stat={alert_show_stat} update_fn={setAlert_Show} desc={Alert_Data} />


      <Modal_Alert show_stat={show_tread_buy} update_fn={setShowTread_buy} desc={Thread_Buy_Render()} ok_callback={Send_Buy_Thread} />

      <IconRegistry icons={EvaIconsPack} />
      <View style={{ flex: 4 }}>
        {render_img_fn()}
      </View>
      <View style={{ flex: 7, marginTop: 10 }}>
        <ScrollView>
          <View style={{ backgroundColor: "#fff", padding: 10 }}>
            <Text category="s1" style={{ borderBottomWidth: 1, borderColor: "#ccc", paddingBottom: 10 }}>共享線程</Text>
            {Thread_Rows}
          </View>


          <TouchableOpacity

            style={[G_Styles.button, { marginBottom: 20, backgroundColor: "#0093e8" }]}

            onPress={Buy_Thread}
          >
            <Text

              style={{ color: "#fff" }}>兑換付費線程</Text>
          </TouchableOpacity>

          <View style={{ backgroundColor: "#fff", padding: 10, marginTop: 10 }}>
            <Text category="s1" style={{ borderBottomWidth: 1, borderColor: "#ccc", paddingBottom: 10 }}>執行進度</Text>
            {Thread_Result}
          </View>
          {/* <View style={{ backgroundColor: "#fff", padding: 10, marginTop: 10 }}>
            <Text category="s1" style={{ borderBottomWidth: 1, borderColor: "#ccc", paddingBottom: 10 }}>執行進度</Text>
            {Last_Run_Worker}
          </View> */}


        </ScrollView>
      </View>

      <Modal
        visible={visible}
        backdropStyle={themedStyle.backdrop}
        onBackdropPress={() => setVisible(false)}>
        <Card disabled={true}>
          <Text>立即填寫SEO網址 😻</Text>
          <Input placeholder="https://.........." multiline={true}
            style={{ height: 100, width: 200, textAlignVertical: 'top', justifyContent: "flex-start" }}
            onChangeText={description => setNow_Fill_Url(description)}
          ></Input>
          <Button onPress={() => store_thread()}>
            送出
          </Button>
        </Card>
      </Modal>


    </>
  );
};



export default SEO_Setting;

const themedStyle = {
  container: {
    flex: 1
  }, button: {
    margin: 2,
  }, backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 16,
    height: 16,
  },
  card: {
    backgroundColor: theme['color-basic-100'],
    marginBottom: 25,

    borderBottomWidth: 0.25,
  },
  cardImage: {
    width: '100%',
    height: 300
  },
  cardHeader: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cardTitle: {
    color: theme['color-basic-1000']
  },
  cardAvatar: {
    marginRight: 16
  },
  cardContent: {
    padding: 10,
    // borderWidth: 0.25,
    borderColor: theme['color-basic-600']
  },
  statusContent: {
    padding: 10,
    borderColor: theme['color-basic-600']
  }
}