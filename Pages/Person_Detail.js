/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  ImageBackground

} from 'react-native';

import { Post_API, Auth_Get_API, Auth_Post_API, Get_API } from "../Component/Host.js";
import { EvaIconsPack } from '@ui-kitten/eva-icons';

import { Modal_Alert } from "../Component/Modal.js"
import { ImgSources } from "../Component/ImgSources.js"

import * as eva from '@eva-design/eva';

import { ApplicationProvider, Layout, Button, Text, Avatar, withStyles, List, IconRegistry, Icon, Input } from '@ui-kitten/components';
import { Image, View, TouchableOpacity } from 'react-native';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { default as theme } from './custom-theme.json'; // <-- Import app theme



const state = { image: null, title: '', description: '' }
function setState(key, title) {
  state[key] = title;
}


const Person_Detail = ({ route, navigation }) => {
  const { person_uid } = route.params;
  const [User_Info, setUserInfo] = useState(null);
  const [add_done_show, setAddDone_Show] = useState(false);
  const [add_done_desc, setAdd_Done_Desc] = useState("");
  const [is_friend, setIs_friend] = useState(false);


  const update_base_info = (e) => {
    e = JSON.parse(e)

    if (e.status == false) {
      return 0;
    }
    var all_data = e.data['user'];
    console.log(all_data);
    setUserInfo(all_data)
  }

  // 打api去確認該會員是不是好友
  useEffect(() => {
    Auth_Get_API(`is-friend/${person_uid}`, (data) => {
      if (data === "1") {
        setIs_friend(true);
      }
    });
  }, [])


  useEffect(() => {
    if (User_Info == null) {
      // Loading_User_Info();
      Get_API('profile/' + person_uid, update_base_info);
    }
  }, [User_Info])


  // function Loading_User_Info() {
  //   function update_base_info(e) {
  //     // console.log(e)
  //     e = JSON.parse(e)

  //     if (e.status == false) {
  //       return 0;
  //     }

  //     var all_data = e.data['user'];
  //     setUserInfo(all_data)

  //   }

  //   Get_API('profile/' + person_uid, update_base_info);
  // }

  function Render_Profile() {
    if (User_Info == null) {
      return <></>
    } else {


      var detail_key_map = { '部門': "part", "職稱": "title", "產業描述": "desc" }

      function Gen_Detail_Data() {
        var detail_labels = Object.keys(detail_key_map);
        var end = [];
        detail_labels.forEach(label => {
          var detail_key = detail_key_map[label];
          var data = User_Info[detail_key]
          var width_size = 1;
          if (detail_key == "desc") {
            width_size = 0;
          }
          var render = (
            <View style={[{ borderBottomWidth: width_size }, styles.detail_row]}>
              <Text style={{ flex: 7 }}>{label}</Text>
              <Text style={{ flex: 20, color: "#555", paddingRight: 30, marginLeft: 30 }}>{data}</Text>
            </View>
          )
          // console.log(render)
          end.push(render)
        });
        return end;
      }
      const Detail_View = Gen_Detail_Data();

      function add_friend() {
        // console.log(Friend_add_phone);

        var cab = function (e) {

          var data = JSON.parse(e)
          // console.log(data)
          if (data.success) {
            setAdd_Done_Desc("加入成功！")
          } else {
            setAdd_Done_Desc("已經申請過好友！")
          }
          setAddDone_Show(true)
        }
        var phone = User_Info.phone;
        var post_data = { 'phone': phone, 'to_id': "0", 'message': "邀請您一同談合作！" }
        Auth_Post_API("send-chat-request", post_data, cab)

        // setFriend_Modalshow(false);
      }
      // 修改自Message_List.js 的 go_to_chat 函式
      function go_to_chat(person_uid) {
        Auth_Get_API("my-contacts", (e) => {
          let data = JSON.parse(e);
          data = data['data']['users'];
          data = data.filter(user => user.id === person_uid);
          let conver_info_json = JSON.stringify(data[0]);
          AsyncStorage.setItem("conver_info", conver_info_json).then(() => {
            navigation.navigate("chatroom");
          })
        });
      }

      function Gen_Add_Friend() {
        if (is_friend) {
          return (
            <View style={{ flex: 1 }}>
              <Button status='primary' style={{ flex: 1 }}
                onPress={() => { go_to_chat(person_uid) }}
              >
                傳送訊息
              </Button>
            </View>)
        }
        return (
          <View style={{ flex: 1 }}>
            <Button status='primary' style={{ flex: 1 }}
              onPress={() => { add_friend() }}
            >
              談合作
            </Button>
          </View>)
      }
      const Add_Friend_Btn = Gen_Add_Friend();
      // console.log(Detail_View)
      return (
        <View style={{ flexDirection: "column" }}>
          <View style={[{ height: 100, flexDirection: "row" }, styles.person_info_box]}>
            <View style={{ flex: 1 }}>
              <Image source={{ uri: User_Info.photo_url }} style={{ width: 60, height: 60, borderRadius: 100 }}></Image>
            </View>

            <View style={{ flex: 5, flexDirection: "column", textAlign: "left", marginLeft: 10 }}>
              <Text category='h5' style={{ flex: 1 }, styles.person_detail_text}>{User_Info.name}</Text>
              <Text category='label' style={{ fontSize: 20, color: "#fff" }, styles.person_detail_text}>地區：{User_Info.country}</Text>
              <Text category='label' style={{ flex: 1 }, styles.person_detail_text}>會員等級：{User_Info.lv}</Text>
              <Text category='label' style={{ flex: 1 }, styles.person_detail_text}>ID:{User_Info.user_id}</Text>
            </View>

          </View>
          <View style={[{ flexDirection: "column", marginTop: 10, flex: 1, padding: 20 }, styles.person_info_box]}>
            {Detail_View}
            {Add_Friend_Btn}
          </View>

        </View>


      )
    }
  }

  return (
    <>
      <View style={{ flex: 1 }}>
        <Modal_Alert show_stat={add_done_show} desc={add_done_desc} update_fn={setAddDone_Show} />

        <ScrollView contentContainerStyle={{}}>
          <View>
            <ImageBackground source={ImgSources.background_color.uri} resizeMode="cover" style={{ width: "100%", height: "100%" }}>

              <View style={{ flex: 1 }}>
                <View style={{ flex: 2 }}>
                  {Render_Profile()}
                </View>




              </View>
            </ImageBackground>
          </View>
        </ScrollView>
      </View>
    </>
  );
};



export default Person_Detail;

const styles = {
  person_detail_text: {
    marginTop: 0,
    fontWeight: "100"
  },
  person_info_box: {
    backgroundColor: "#fff",
    paddingLeft: 10,
    alignItems: 'center',
  },
  detail_row: { flex: 1, flexDirection: "row", borderColor: "#eee", paddingBottom: 10, paddingTop: 10 }
}