
import React, { useState,useEffect } from 'react';

import {
    StyleSheet,
    Text,
    Image,
    View,
    Button,
    TouchableOpacity,
    TextInput,
    Modal,

    Alert,
    TouchableHighlight
  } from 'react-native';
const Modal_Alert = (props) => {



  function close_modal(){
    props.update_fn(!props.show_stat);
    if(props.ok_callback != undefined){
      props.ok_callback();
    }
  }    
  // console.log(typeof(props.desc))
if(typeof(props.desc) == 'string'){
  var desc_DOM =   <Text style={styles.modalText}>{props.desc}</Text>
}else{
  var desc_DOM =  props.desc;
}



    return (

<Modal
        animationType="fade"
        transparent={true}
        visible={props.show_stat}
        onRequestClose={() => {
          // Alert.alert("Modal has been closed.");
          try {
            setModalVisible(!modalVisible);  
          } catch (error) {
            
          }
          
        }}
        
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          {desc_DOM}
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={close_modal}
            >
              <Text style={[styles.textStyle,styles.okStyle]}>OK!</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>

    );
  };

  const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    okStyle:{
      padding:10
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });
  
  export {Modal_Alert}