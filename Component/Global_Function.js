
  const updateForm_Info = (key, val,setState_fn,origin)=> {
    
    var end_val = val;
    if(typeof(val) == 'object'){
      var first_key = Object.keys(val)[0];
      end_val = val[first_key];
    }
    origin[key] = end_val;
    setState_fn(origin)
   

  }

  export {updateForm_Info}