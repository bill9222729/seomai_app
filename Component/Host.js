
import AsyncStorage from '@react-native-async-storage/async-storage';
// const Host_URL = "https://semai.ddrising.net/";
const Host_URL = "https://box.somai.app/";


const Auth_Get_API = (router_name, success_callback, fail_callback = null) => {
  AsyncStorage.getItem('token').then((token) => {
    // console.log("token",token)
    Get_API(router_name = router_name, success_callback = success_callback, fail_callback = fail_callback, token = token)
  })

}

const Get_API = (router_name, success_callback, fail_callback = null, token = null) => {


  var url = Host_URL + "api/" + router_name;
  var ignore_list = ['/conversation', 'user_stories', 'seo_thread_managers', 'seo_mission_update', 'my-wallet', 'my-chat', 'my-contacts']
  var is_ignore_out = false;
  ignore_list.forEach(element => {
    if (url.indexOf(element) != -1) {
      is_ignore_out = true;
    }
  });
  if (is_ignore_out == false) {
    console.log('start ' + url)
  }


  fetch(url, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer ' + token,
      'Cache-Control': "no-cache",
      'Pragma': 'no-cache',

    })

  }).then((response) => {

    // console.log("response")
    // console.log(response)
    return response.text()
  }).then((jsonData) => {

    // console.log(jsonData);
    if (success_callback != null) {
      // console.log('callback')
      // console.log(jsonData)
      success_callback(jsonData);
    }
  }).catch((err) => {
    console.log(url)
    console.log('GET API 錯誤:', err);
    if (fail_callback != null) {
      fail_callback(err)
    }
  })
}

const Auth_Post_API = (router_name, post_data, success_callback, fail_callback = null) => {
  AsyncStorage.getItem('token').then((token) => {

    Post_API(router_name = router_name, post_data = post_data, success_callback = success_callback, fail_callback = fail_callback, token = token)
    // console.log(token)


  })

}
const Post_API = function (router_name, post_data, success_callback, fail_callback = null, token = null) {
  var url = Host_URL + "api/" + router_name;

  var ignore_list = ['/conversation', 'user_stories', 'seo_thread_managers', 'seo_mission_update', 'my-wallet', 'my-chat', 'my-contacts']
  var is_ignore_out = false;
  ignore_list.forEach(element => {
    if (url.indexOf(element) != -1) {
      is_ignore_out = true;
    }
  });
  if (is_ignore_out == false) {
    console.log('start ' + url)
  }

  let formdata = new FormData();
  var keys = Object.keys(post_data);
  // console.log(post_data)
  for (let index = 0; index < keys.length; index++) {
    var key = keys[index];
    var val = post_data[key];
    //  console.log(key)
    //  console.log(val)
    if (val.hasOwnProperty('fileName') && val.hasOwnProperty('uri')) {
      //檔案上傳
      var file_val = { uri: val.uri, 'filename': val.fileName, 'type': val.type, name: val.fileName }
      formdata.append(key, file_val);
      //  console.log(file_val)
    } else {
      formdata.append(key, val);
    }


  }
  // formdata.append('name','testapp');
  // formdata.append('email','testapp@gmail.com');
  // formdata.append('password','Aa5161651asdf');
  fetch(url, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer ' + token,
      'Cache-Control': "no-cache",
      'Pragma': 'no-cache',
    }),
    body: formdata
  }).then((response) => {

    // console.log("response")
    // console.log(response)
    return response.text()
  }).then((jsonData) => {

    // console.log(jsonData);
    if (success_callback != null) {
      success_callback(jsonData);
    }
  }).catch((err) => {

    console.log('POST_API錯誤:', err);
    if (fail_callback != null) {
      fail_callback(err)
    }
  })
}


// }
export { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL };