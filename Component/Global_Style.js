import {
    StyleSheet,
  } from 'react-native';

  
const G_Styles = StyleSheet.create({
    container:{
        flex:1,
        padding:15
    },
    
    sub_box:{
        
        alignItems: "center",
        },
    background_gary:{
       
    backgroundColor: "#f0f5f9"
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
      },

    primary_blue:{
        backgroundColor: "#0093e8"
    }

});  
   
// const G_Styles = {
//     container:{flex:1,padding:10},
// }
export { G_Styles };