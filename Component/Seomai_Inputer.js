import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    Button,
    TouchableOpacity,

    TextInput
} from 'react-native';

import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { G_Styles } from "../Component/Global_Style.js"
const SeoMai_Inputer = (props) => {

    const [input_val, setInputVal] = useState(null);
    const [input_type, setInputType] = useState(props.input_type);
    const [now_select_photo, setSelectPhoto] = useState(null);
    function onchange_val(e) {

        setInputVal(e)
        if (props.update_fn != null) {
            props.update_fn(props.name, e);
        }
        if (input_type == 'photo') {
            setSelectPhoto(e);
        }
    }
    const selectImage = () => {
        const options = {
            noData: true
        }
        // console.log(launchImageLibrary)
        launchImageLibrary(options, response => {
            if (response.didCancel) {
                console.log('User cancelled image picker')
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error)
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton)
            } else {
                const source = { uri: response.assets[0].uri }
                // console.log(response.assets[0])

                onchange_val(response.assets[0])

                // setImagePreview(source)
                // setState("image", response.assets[0])
                // console.log(image_preview)
            }
        })
    }


    function Render_Input() {
        var end = [];
        if (input_type == 'text' || input_type == null) {
            end.push(<TextInput
                style={styles.input}
                value={input_val}
                onChangeText={onchange_val}
                placeholder={props.placeholder}
                keyboardType={props.keyboardType}
            />)
        }

        if (input_type == 'photo') {

            if (now_select_photo != null) {

                end.push(<Image source={now_select_photo} style={{ width: "100%", height: 300, borderWidth: 1, backgroundColor: "#000", padding: 10 }} />)
            }


            end.push(<TouchableOpacity

                style={[G_Styles.button, { marginTop: 10, backgroundColor: "#B671FF" }]}
                onPress={selectImage}
            >
                <Text

                    style={{ color: "#fff" }}>選擇照片</Text>
            </TouchableOpacity>)

        }

        return end;
    }

    return (

        <View style={styles.main_box}>
            <Text style={{ color: "#000", padding: 0, marginLeft: 10 }}>{props.title}</Text>
            {Render_Input()}
        </View>
    )
}


const styles = StyleSheet.create({
    main_box: {
        borderBottomWidth: 1,
        borderBottomColor: "#ccc"
    },
    input: {
        height: 40,
        borderWidth: 0,
        padding: 10,
    },
});


export { SeoMai_Inputer }