

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  useWindowDimensions,
  ScrollView,
  StatusBar,
  Linking

} from 'react-native';

import { ApplicationProvider, Layout, Button, Text, Avatar, Modal, Input, IconRegistry, Icon, Card } from '@ui-kitten/components';
import { Image, View, TouchableOpacity } from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Post_API, Get_API, Auth_Get_API, Auth_Post_API, Host_URL } from "../Component/Host.js"
const storeData = async (key,value) => {
  try {
   
    if(typeof(value) != "string") {
      value = JSON.stringify(value)
    }
    // console.log("store:",key,value)
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // console.log('save error')
    // console.log(e)
    // saving error
  }
}

function SEO_Thread() {
  

  const [seo_work_data, set_SEO_Work] = useState(null);
  const [now_seo_num,setNow_SEO_Num] = useState(0);
  const [now_ready_seo,setNow_Ready_SEO] = useState(false);
  
  
  function Load_Seo_Thread() {
    function cab(e) {
      // console.log(e);
      set_SEO_Work(JSON.parse(e))
      
      storeData('seo_list',JSON.parse(e));
    }
    Auth_Get_API('seo_worker', cab)
  }
  function Update_Seo_Mission(user_id, type, num) {
    function cab(e) { 
      // console.log(e)
     };

    var post_data = { user_id: user_id, 'type': type, 'num': num }
    Auth_Post_API("seo_mission_update", post_data, cab)
  }

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  // function Start_SEO_Work() {
  const Start_SEO_Work = async () => {
    if (seo_work_data == null || seo_work_data.length == 0) { return; }
    // console.log("shuffer",shuffer)
    if(now_ready_seo){
      return;
    }
    await AsyncStorage.getItem('token').then((token) => {
      console.log('hello')
      console.log(token);
      if (token !== null) {
      
    setNow_Ready_SEO(true)
    var runing_num = -1;
    var reload_seo_thread = Load_Seo_Thread;

    setInterval(function () {
      try {
           // console.log('first_now_seo_num',runing_num)
      var new_n = runing_num+1;
      // console.log('new_n',new_n)
      if(new_n > seo_work_data.length){
        new_n = 0;
        console.log("max_count 0")
        console.log('reload links')
        reload_seo_thread();
      }
      // change_Now_SEO_Num(5);
      runing_num = new_n;
      storeData('seo_num',runing_num)
      // var shuffer = seo_work_data.sort(() => Math.random() - 0.5);
      // console.log(seo_work_data,now_seo_num,new_n)
      var mission = seo_work_data[runing_num];
      console.log('mission')
      console.log(mission)
      if(mission != undefined){
      storeData('now_work_seo_url',runing_num);
      // console.log("Store Now URL",mission['url'])
      fetch(mission['url'], {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'multipart/form-data',
          'Cache-Control': "no-cache",
          'Pragma': 'no-cache',

        })

      }).then((response) => {

        // console.log('work_done')
        Update_Seo_Mission(mission['user_id'], mission['type'], mission['num'])

      });
      }
      } catch (error) {
        console.log(error)
      }
   
    
    }, getRandomArbitrary(20,60)*1000)
      } else {
    return;
        
      }
    })



  // }, getRandomArbitrary(1,3)*1*1000)

  }
 

  if (seo_work_data == null) {
    Load_Seo_Thread();
  }
  //
  React.useEffect(() => {
    Start_SEO_Work()
  }, [seo_work_data]);

  return (<View></View>)
}

export { SEO_Thread };