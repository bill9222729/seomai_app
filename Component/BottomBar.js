import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { ImgSources } from "./ImgSources.js"
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';


const Bottom_Head = ({
  icon_name
}) => {

  const imgSource = ImgSources[icon_name].uri;
  // console.log(imgSource)
  return <Image
    style={styles.tinyLogo}
    source={imgSource}
  />
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#576274",
  },
  bottom_view: {

    flex: 1,
    paddingLeft: 10,
    width: "100%",
    height: "100%",
    alignItems: "center",
  },
  tinyLogo: {

    // width: "50%",
    // height: "50%",
    width: 20,
    height: 20,
    // transform: [{ scale: 0.5 }]
  },
  bottom_text: {
    fontSize: 10,
    color: "#ffffff",
    textAlign: "center",
    fontWeight: "100"
  },
  button: {
    alignItems: "center",
    // backgroundColor: "#DDDDDD",
    padding: 10
  },

});



var Icon_Btn_List = [
  { 'name': 'home', 'nick': "首頁" },
  { 'name': 'messenger', 'nick': "訊息" },
  { 'name': 'directory', 'nick': "通訊錄" },
  { 'name': 'mai_sercive', 'nick': "SEO" },
  { 'name': 'member', 'nick': "會員" }];

// var Icon_Btn_List = ['home'];

const BottomBar = () => {
  const navigation = useNavigation();

  function Reset_Loading() {

    var keys = [setHomeIsloadingImg];

    for (let index = 0; index < keys.length; index++) {
      try {
        const element = keys[index];
        element(false)

      }
      catch (error) {

      }

    }
  }
  const jump_page = async (name) => {
    try {
      
      // setMsgIsLoading(false)
      // Reset_Loading();
    } catch (error) {

    }
    await AsyncStorage.getItem('token').then((token) => {
      // console.log('hello')
      // console.log(token);
      if (token !== null) {
        // console.log('login_success')
        navigation.navigate(name);

      } else {
        if(name != "home"){
          navigation.navigate("Login");
        }else{
          navigation.navigate("home");
        }
        
      }
    })


  }

  function Gen_Head_Icon() {
    var n = 0;
    var items = Icon_Btn_List.map(item => {
      //  console.log(item)
      n++;
      return <View key={n} style={[styles.bottom_view]} >
        <TouchableOpacity
          style={styles.button}
          onPress={() => { jump_page(item.name) }}
        ><Bottom_Head icon_name={item.name} />
          <Text style={styles.bottom_text}>{item.nick}</Text>
        </TouchableOpacity>

      </View>
    });
    // console.log(items)
    return items;
  }
  const Head_List_Render = Gen_Head_Icon();
  return (
    <View style={[styles.container, {
      // Try setting `flexDirection` to `"row"`.
      flexDirection: "row"
    }]}>


      {Head_List_Render}
    </View>
  );
};


export default BottomBar;