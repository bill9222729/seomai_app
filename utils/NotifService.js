// 引入套件
import PushNotification from 'react-native-push-notification';

// 設定config
PushNotification.configure({
  popInitialNotification: true,
  requestPermissions: true,
  
  // 使用remote遠端通知的話，一定要先註冊，然後取得token
  onRegister: function (token) {
    PushNotification.checkPermissions(({ alert, badge, sound }) => {
      console.log(alert,badge, sound)
      // if (!alert || !badge || !sound) {
      //   PushNotification.requestPermissions();
      // }
      });
  
    // PushNotification.requestPermissions();
    console.log('TOKEN:', token);
  },
  onRegistrationError: function (err) {
    console.error(err.message, err);
  }
});

// 輸出方法
export default {
  getChannelExists(channelId) {
    return new Promise((resolve, reject) => {
      PushNotification.channelExists(channelId, function (exists) {
        resolve(exists);
      });
    });
  },
  createChannel(channelId) {
    PushNotification.createChannel(
      {
        channelId: channelId,
        channelName: channelId
      },
      created => console.log(`createChannel ${channelId} returned '${created}'`)
    );
  }
};