/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type {Node} from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   Navigator,
 } from 'react-native';
 
 import {
   Colors, 
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';
 import home from './Pages/home.js';
 
 import Flex from "./Component/test.js"
 import BottomBar from "./Component/BottomBar.js"
 const Section = ({children, title}): Node => {
   const isDarkMode = useColorScheme() === 'dark';
   return (
 
 
     
     <View style={styles.sectionContainer}>
       <Text
         style={[
           styles.sectionTitle,
           {
             color: isDarkMode ? Colors.white : Colors.black,
           },
         ]}>
         {title}
       </Text>
       <Text
         style={[
           styles.sectionDescription,
           {
             color: isDarkMode ? Colors.light : Colors.dark,
           },
         ]}>
         {children}
       </Text>
     </View>
   );
 };
 
 const App: () => Node = () => {
   const isDarkMode = useColorScheme() === 'dark';
 
   const backgroundStyle = {
     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
   };
   
 
   return (
     <SafeAreaView style={{flex:1}}>
    
       {/* <Flex></Flex> */}
   
       <View style={ {
       // Try setting `flexDirection` to `"row"`.
       flex: 1,
       flexDirection: "column"
     }}>
 
       <View style={{ flex: 2, backgroundColor: "red" }} />
       <View style={{ flex: 20, backgroundColor: "darkorange" }} />
       <View style={{ flex: 2}}>
       
       <BottomBar />
      
       
       </View>
 
 
       </View>
       
 
     </SafeAreaView>
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },
   container: {
     flex: 1,
   },
 });  
 
 export default App;
 